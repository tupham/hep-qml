#!/bin/bash

new_job_file=new_job.sh

git fetch
git pull

if [ -f "$new_job_file" ]; then
    echo "run new job: $new_job_file"

    # remove the file to avoid rerun it
    torun_new_job_file=${new_job_file}"_to_run.sh"
    cp $new_job_file ${torun_new_job_file}
    git rm $new_job_file
    git commit -m 'rm new_job.sh'
    git push origin master

    # run the job
    # bash $torun_new_job_file > /dev/null 2>&1
    bash $torun_new_job_file

    # copy outputs
    python copy_outputs.py
fi
