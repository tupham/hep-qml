# generate numpy array of desired training variables for sqvm and xgboost

#import uproot
import pandas as pd, numpy as np, os
from utils.parameters import RANDOM_SEED, NUMBER_OF_EVENTS, sample_list, ZP2HDM, A2HDM
#from run_xgboost import further_selections
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from root_pandas import read_root, to_root
from sklearn.utils import shuffle
from argparse import ArgumentParser
import os

def getArgs():
	parser = ArgumentParser()
	parser.add_argument('--inputdir', action='store', default='data/training_data_monohbb',
						help='Directory which contains training samples')
	parser.add_argument('--region', action='store', choices=['Merged', 'Merged_500_900', 'Merged_900', 'Resolved',
															 'Resolved_150_200', 'Resolved_200_350', 'Resolved_350_500'],
						default='Resolved', help='Region to further select data')
	parser.add_argument('--save_npy', action='store', default='monohbb_100_events',
						help='Name to save data numpy array.')
	parser.add_argument('--save_to', action='store', default='monohbb_100_events',
						help='Directory to save data numpy array.')
	parser.add_argument('--zp2hdm', type=int, nargs='+', default = [],
						help='Masspoint of training ZP2HDM model samples.')
	parser.add_argument('--tHDMa_ggF', type=int, nargs='+', default = [],
						help='Masspoint of training 2HDM+a model samples (with ggF production).')
	parser.add_argument('--tHDMa_bb', type=int, nargs='+', default = [],
						help='Masspoint of training 2HDM+a model samples (with bb production).')
	parser.add_argument('--vars',action='store',nargs='+',
						default=['MetTST_met', 'N_Jets04', 'HtRatioResolved','Dijet_pt','Dijet_eta','PCS_2','PCS_1',
								 'Delta_phi'],
						help='Training variables')
	parser.add_argument('--bkg',action='store',nargs='+',choices=['Z','W','ttbar','VH','diboson','stop'],
						default=['Z','W','ttbar','VH','diboson','stop'],
						help='Background categories to include in the training')
	parser.add_argument('--event_number', action='store', default=100, type=int, help='Number of events in training and test samples')
	parser.add_argument('--number_of_samples', action='store', type=int, default=1, help='Number of samples to generate')
	parser.add_argument('--TBranch', action='store', required=False, help='TBranch in the root file from which to read data')
	return parser.parse_args()

args=getArgs()


def further_selections(data, region):
	data_ = data
	if not 'Resolved' in region:
		data_ = data_[data_.MetTST_met > 500000.]
		if region in ['Merged','Merged_900']:
			data_=data_[data_.HtRatioMerged <= 0.57]
		if region == 'Merged_500_900':
			data_=data_[(data_.MetTST_met >  500000.) & (data_.MetTST_met<=900000.)]
		elif region!='Merged':
			data_ = data_[data_.MetTST_met > 900000.]
	else:
		data_ = data_[(data_.MetTST_met > 150000.) & (data_.MetTST_met < 500000.)]
		if region == 'Resolved_150_200':
			data_ = data_[(data_.MetTST_met >= 150000.) & (data_.MetTST_met < 200000.)]
		elif region == 'Resolved_200_350':
			data_ = data_[(data_.MetTST_met >= 200000.) & (data_.MetTST_met < 350000.)]
		elif region == 'Resolved_350_500':
			data_ = data_[(data_.MetTST_met < 500000.) & (data_.MetTST_met >= 350000.)]
	return data_


def load_root(inputdir, bkg_categories, sig_categories, region, vars, save_npy, save_to, event_number = NUMBER_OF_EVENTS, TBranch=None, number_of_samples=1):

	# define branch to read data from the ROOTDirectory according to the region
	if not TBranch:
		branch = 'Resolved' if region[0] == 'R' else 'Merged'
	else:
		branch = TBranch

	# create a dict containing the data
	data_dict = {'bkg': {'categories': bkg_categories, 'df': pd.DataFrame()}, 
				'sig': {'categories': sig_categories, 'df': pd.DataFrame()}}

	# load all data from flat root to dataframe and do further selections
	for event_type in data_dict:
		for category in sorted([cat for cat in data_dict[event_type]['categories'] if cat in os.listdir(inputdir)]):
			cat_dir = inputdir + '/' + category
			print('*' * 10 + 'Loading %s' % cat_dir + '*' * 10)
			dataframe = pd.DataFrame()
			for root in sorted([file for file in os.listdir(cat_dir) if (file.endswith('.root') and 'empty' not in file)]):
				try:
					for chunk in read_root(paths=cat_dir + '/' + root,
										   key=branch,
										   columns=list(set(vars) | set(['MetTST_met', 'HtRatioMerged'])),
										   chunksize=1000):
						dataframe = dataframe.append(chunk, ignore_index=True)
				except:
					print('Unable to open %s' % cat_dir + '/' + root + ', opening the next file.')
					continue

			dataframe['category'] = category
			if not dataframe.empty:
				# further selections
				dataframe = further_selections(data=dataframe, region=region)[vars + ['category']]
			else:
				continue

			# append a new column containging the weights for selection. The weight of an event of some category =
			dataframe['selection_weights'] = 1. / dataframe.shape[0]
			#print(dataframe.head())
			data_dict[event_type]['df'] = pd.concat([data_dict[event_type]['df'], dataframe], ignore_index=True)

	#print(data_dict['bkg']['df'].head())
	#print(data_dict['sig']['df'].head())
	min_event_number = min(event_number, data_dict['bkg']['df'].shape[0], data_dict['sig']['df'].shape[0])
	print('Background data size: {},\nSignal data size: {}'.format(data_dict['bkg']['df'].shape[0], data_dict['sig']['df'].shape[0]))
	if event_number > min_event_number:
		print('WARNING: Insufficient number of events, sampling the least number of events in dataframes, {}!'.format(min_event_number))
	# randomly select the desired number of events weighted equally among all categories
	# sss = StratifiedShuffleSplit(n_splits=number_of_samples, train_size=min_event_number, random_state=RANDOM_SEED)
	for event_type in data_dict:
		np.random.seed(RANDOM_SEED)
		random_states=np.random.randint(low=0, high=1000, size=number_of_samples)
		m=0
		for random_state in random_states:
			data_dict[event_type]['random_sample_{}'.format(m)] = data_dict[event_type]['df'].sample(n=min_event_number,
																									replace=False,
																									weights=data_dict[event_type]['df']['selection_weights'],
																									random_state=random_state
																									)
			data_dict[event_type]['random_sample_{}'.format(m)]['Y'] = 0 if event_type == 'bkg' else 1
			m+=1
		#data_dict[event_type]['training_sample'], data_dict[event_type]['testing_sample'] = train_test_split(data_dict[event_type]['random_sample'],
		#                                                                                                    test_size=0.5,
		#                                                                                                    random_state=RANDOM_SEED)
	category_statistics={}
	for i in range(number_of_samples):
		data_dict['combined_df_{}'.format(i)] = pd.concat([data_dict['sig']['random_sample_{}'.format(i)], data_dict['bkg']['random_sample_{}'.format(i)]], ignore_index=True) 
		category_statistics[i]={category: data_dict['combined_df_{}'.format(i)][data_dict['combined_df_{}'.format(i)]['category'] == category].shape[0] for category in bkg_categories+sig_categories}
		data_dict['combined_df_{}'.format(i)] = data_dict['combined_df_{}'.format(i)][vars+['Y']]
		output = shuffle(data_dict['combined_df_{}'.format(i)].to_numpy(), random_state = RANDOM_SEED)
		
		print('*' * 10 + ' Saving data to numpy sample {}'.format(i) + '*' * 10)
		if not os.path.isdir(save_to): os.makedirs(save_to)
		np.save(os.path.join(save_to, save_npy+'_{}'.format(i)), output)

	return category_statistics

def main(args):
	inputdir = args.inputdir
	number_of_events = args.event_number
	region = args.region
	vars = args.vars
	save_npy = args.save_npy
	save_to = args.save_to
	#bkg_categories = [cat for cat in sample_list.keys() if cat != 'data']
	bkg_categories = args.bkg
	if args.TBranch: TBranch = args.TBranch
	else: TBranch = None
	sig_categories = []
	number_of_samples = args.number_of_samples if args.number_of_samples else 1
	if len(args.zp2hdm) % 2 != 0 or len(args.tHDMa_ggF) % 2 != 0 or len(args.tHDMa_bb) % 2 != 0:
		raise SystemExit('Mass points of signal models must contain exactly two points!')

	for sig in np.reshape(args.zp2hdm,[int(len(args.zp2hdm)/2),2]):
		sig_categories.append('zp2hdmbbmzp'+ str(sig[0])+ 'mA' + str(sig[1]))
	for sig in np.reshape(args.tHDMa_ggF,[int(len(args.tHDMa_ggF)/2),2]):
		sig_categories.append('2HDMa_ggF_tb10_sp035_mA'+ str(sig[0])+ '_ma' + str(sig[1]))
	for sig in np.reshape(args.tHDMa_bb,[int(len(args.tHDMa_bb)/2),2]):
		sig_categories.append('2HDMa_bb_tb10_sp035_mA'+ str(sig[0])+ '_ma' + str(sig[1]))

	category_statistics = load_root(inputdir=inputdir, bkg_categories=bkg_categories, sig_categories=sig_categories,
								region=region, vars=vars, save_npy=save_npy, save_to=save_to, event_number=number_of_events, number_of_samples=number_of_samples, TBranch=TBranch)
	print('Following is the number of events in each category: ')
	for sample_number in category_statistics:
		print('Sample number {}:'.format(sample_number))
		for category in category_statistics[sample_number]: print(category,': ',category_statistics[sample_number][category])
		print('Number of background events: ', np.sum([category_statistics[sample_number][category] for category in bkg_categories]))
		print('Number of signal events: ', np.sum([category_statistics[sample_number][category] for category in sig_categories]))
		print('_'*20)

	return

if __name__ == '__main__':
	main(args)







