#!/usr/bin/env bash
for event_number in 100 200 400 600
do 
    python sample_generator.py --inputdir data/monohbb_train_data --region Resolved --zp2hdm 1400 600 --bkg W Z ttbar --vars MetTST_met N_Jets04 HtRatioResolved Dijet_pt Dijet_eta PCS_2 PCS_1 Delta_phi --save_to data/samples_1400-600_signal_mass_point/${event_number}_events --save_npy monohbb_${event_number}_events --event_number=${event_number} --number_of_samples 30
done