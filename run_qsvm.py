#!/usr/bin/env python

import os
import traceback
import json
import argparse
import sys
import logging
import time
import datetime
import numpy as np
import pandas as pd

#from utils.load_data import load_events_npy, load_npz_data
from utils.load_data import load_events_npy, load_npz_data

from algorithms.qsvm_new import QSVMDev
from sklearn import svm
from sklearn.metrics import roc_curve, auc, confusion_matrix, accuracy_score,roc_auc_score
from sklearn.model_selection import GridSearchCV
from sklearn.utils import shuffle

from qiskit import BasicAer
from qiskit import IBMQ, Aer
from qiskit.aqua import QuantumInstance, aqua_globals
from qiskit.aqua.components.feature_maps import PauliExpansion, FirstOrderExpansion, SecondOrderExpansion
from qiskit.circuit.library import ZFeatureMap, ZZFeatureMap
from qiskit.aqua.utils import split_dataset_to_data_and_labels
from utils.parameters import RANDOM_SEED, plotROC
from sklearn.model_selection import train_test_split, ShuffleSplit

parser = argparse.ArgumentParser()
parser.add_argument('--input', action='store', help='input data', required=True)
parser.add_argument('--kernel_input', action='store', help='input kernel matrix', required=False)
parser.add_argument('--training_size', action='store', type=int, default=10000, help='number of events to be selected for training')
parser.add_argument('--test_size', action='store', type=int, default=100000, help='number of events to be selected for test')
parser.add_argument('--nqubits', action='store', type=int, default=5, help='number of qubits')
parser.add_argument('--with_pca', action='store_true', default=False, help='Whether to use PCA')
parser.add_argument('--with_kernel_pca', action='store_true', default=False, help='Whether to use kernel PCA')
parser.add_argument('--with_standardscale', action='store_true', default=False, help='Whether to use StandardScale')
parser.add_argument('--shots', action='store', type=int, default=1024, help='number of shots')
parser.add_argument('--feature_map', action='store', default='SecondOrderExpansion', help='feature map method')
parser.add_argument('--entanglement', action='store', default=None, help='entanglement')
parser.add_argument('--feature_map_depth', action='store', type=int, default=3, help='feature map depth')
parser.add_argument('--batch_size', action='store', type=int, default=200, help='batch size')
parser.add_argument('--backend', action='store', default='qasm_simulator', help='backend name')
parser.add_argument('--name', action='store', default='test_qnn', help='problem name')
parser.add_argument('--noise', action='store_true', default=False, help='introducing noise model')
parser.add_argument('--noise_model', action='store', help='noise model', required=False)
parser.add_argument('--cat', action='store', default='tth', help='tth, zerojet, onejet, twojet')
parser.add_argument('--with_error_mitigation', action='store_true', default=False, help='Enable error mitigation')
parser.add_argument('--save_roc_plot', action='store',required=False,help='Specify the directory to save roc plots')
parser.add_argument('--save_results', action='store',required=False, help='Specify the directory to save result npz files')
parser.add_argument('--withPolyFeature', action='store_true', default=False, help='Add polynomial features to input data')
parser.add_argument('--evaluation_rounds', action='store', type=int, required=False, help='Number of kernel matrix evaluations')

args = parser.parse_args()

##Default Params##
default_params = {
		'problem': {'name': 'vqc', 'random_seed': RANDOM_SEED},
		'loss_function':{'name': 'default'},
		'algorithm': {'name': 'QSVM.Variational', 'max_evals_grouped':2, 'batch_mode': True, 'minibatch_size': 2},
		'backend': {'provider': 'qiskit.BasicAer', 'name': 'qasm_simulator', 'shots': 1024},
		'optimizer': {'name': 'SPSA', 'max_trials': 30, 'save_steps': 1},
		'variational_form': {'name': 'RYRZ', 'depth': 3, 'num_qubits': 5},
		'feature_map': {'name': 'SecondOrderExpansion', 'depth': 2, 'num_qubits': 5, 'entangler_map': None}
}

##Logging##
def get_log_file_name(file, name, args):
	log_dir = os.path.join(os.path.dirname(os.path.realpath(file)), 'logs')
	input_file = args.input.split('/')[-1] if '/' in args.input else args.input
	input_file = input_file.split('.')[0]
	if not os.path.exists(log_dir):
		os.makedirs(log_dir)
	log_filename = "%s_%s_train%s_test%s_qubits%s_pca%s_polyfeature%s_sscale%s_shots%s_feat%s_depth%s_entan%s_backend%s.log" % (name,
																													input_file,
																													args.training_size,
																													args.test_size,
																													args.nqubits,
																													args.with_pca,
																													args.withPolyFeature,
																													args.with_standardscale,
																													args.shots,
																													args.feature_map,
																													str(args.feature_map_depth),
																													args.entanglement,
																													args.backend)
	return os.path.join(log_dir, log_filename)

def config_logging(params):
	log_filename = get_log_file_name(__file__, params['problem']['name'], args)

	logging.basicConfig(
		level=logging.INFO,
		format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
		handlers=[
			logging.FileHandler("{0}".format(log_filename)),
			logging.StreamHandler()
		])
	return log_filename

def main(args):
	########preparing###############
	params = default_params

	current_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M-%S')
	params['problem']['name'] = args.name + "_" + current_time
	params['problem']['random_seed'] = 10598

	#quantum_instance
	params['backend']['shots'] = args.shots
	params['backend']['name']  = args.backend

	#global seed
	aqua_globals.random_seed = params['problem']['random_seed']

	if args.noise:
		IBMQ.load_accounts()
		device = IBMQ.get_backend('ibmq_16_melbourne')
		properties = device.properties()
		from qiskit.providers.aer import noise
		full_noise_model = noise.device.basic_device_noise_model(properties=properties)

		noise_model = noise.utils.remap_noise_model(full_noise_model,
												remapping=[i for i in range(1,args.nqubits+1)],
												discard_qubits=True)
	else:
		noise_model=None

	basis_gates = None
	red_noise_model = None
	initial_layout = None

	if args.noise_model:
		if args.noise_model.endswith(".npz"):
			nm = np.load(args.noise_model, allow_pickle=True)
			nm_noise = nm['noise_model'].item()
			noise_model = noise.NoiseModel.from_dict(nm_noise)

			coupling_map = nm['coupling_map']
			coupling_map = coupling_map.tolist()
			CMAP = CouplingMap(coupling_map)

			target_qubits = [0,1,2,3,4]
			red_cmap = CMAP.reduce(target_qubits)
			red_noise_model = noise.utils.remap_noise_model(noise_model,
														remapping=target_qubits,
														discard_qubits=True)
			basis_gates = red_noise_model.basis_gates
		else:
			import pickle
			with open(args.noise_model, 'rb') as f:
				 noise_model = pickle.load(f)
			red_noise_model = noise_model
			basis_gates = noise_model.basis_gates

	if args.with_error_mitigation:
	   measurement_error_mitigation_cls = CompleteMeasFitter
	   cals_matrix_refresh_period = 60

	#if args.backend.startswith("ibmq_paris"):
	#   if args.nqubits == 5:
	#      initial_layout = {(qr, 0): 0, (qr, 1): 1, (qr, 2): 2, (qr, 3): 3, (qr, 4): 5}
	#   elif args.nqubits == 10:
	#      initial_layout = {(qr, 0): 0, (qr, 1): 1, (qr, 2): 2, (qr, 3): 3, (qr, 4): 5, (qr, 5): 8, (qr, 6): 11, (qr, 7): 14, (qr, 8): 13, (qr, 9): 12}

	if 'ibmq' in args.backend:
	   from utils import Qconfig
	   try:
		   IBMQ.enable_account(Qconfig.APItoken, Qconfig.config['url'])
	   except:
		   pass
	   my_provider = IBMQ.get_provider(hub=Qconfig.config['hub'], group=Qconfig.config['group'], project=Qconfig.config['project'])
	   #my_provider = IBMQ.get_provider()

	   quantum_instance = QuantumInstance(my_provider.get_backend(name=params['backend']['name']),
									   shots=params['backend']['shots'],
									   basis_gates=basis_gates,
									   noise_model=red_noise_model,
									   seed_simulator=params['problem']['random_seed'],
									   seed_transpiler=params['problem']['random_seed'],
									   initial_layout=initial_layout,
									   optimization_level=1)

	else:
	   quantum_instance = QuantumInstance(Aer.get_backend(params['backend']['name']),
										  shots=params['backend']['shots'],
										  basis_gates=basis_gates,
										  noise_model=red_noise_model,
										  initial_layout=initial_layout,
										  seed_simulator=params['problem']['random_seed'],
										  seed_transpiler=params['problem']['random_seed'])

	#feature_map
	params['feature_map']['name'] = args.feature_map
	params['feature_map']['depth'] = args.feature_map_depth
	if args.entanglement == 'linear':
	   entangler_map = []
	   for i in range(0, args.nqubits - 1, 2):
		   entangler_map.append([i, i + 1])
	   for i in range(0, args.nqubits - 2, 2):
		   entangler_map.append([i + 1, i + 2])
	   params['feature_map']['entangler_map'] = entangler_map

	if args.entanglement == 'full':
	   params['feature_map']['entangler_map'] = [[i, j] for i in range(args.nqubits) for j in range(i + 1, args.nqubits)]
	if params['feature_map']['name'] == 'SecondOrderExpansion':
	   feature_map = ZZFeatureMap(feature_dimension=args.nqubits, reps=args.feature_map_depth, entanglement=args.entanglement)
	if params['feature_map']['name'] == 'FirstOrderExpansion':
	   feature_map = ZFeatureMap(feature_dimension=args.nqubits, reps=args.feature_map_depth)

	if params['feature_map']['name'] == 'FeatureMap7':
	   from algorithms.feature_map7 import FeatureMap7
	   feature_map = FeatureMap7(num_qubits=args.nqubits, depth=args.feature_map_depth)
	if params['feature_map']['name'] == 'FeatureMap7Dev':
	   from algorithms.feature_map_7dev import FeatureMap7Dev
	   feature_map = FeatureMap7Dev(num_qubits=args.nqubits, depth=args.feature_map_depth)

	####################################
	log_filename = config_logging(params)
	#input_id = args.input.split('_')[-1].replace('.npy','')
	#log_filename = log_filename.replace(args.name, args.name+f'_{input_id}')
	logging.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
	logging.info("+++++++ execution: %s ++++++++++" % params['problem']['name'])
	logging.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
	logging.info("log file: %s" % log_filename)

 
	logging.info("backend: %s, shots: %s" % (args.backend, str(args.shots)))
	logging.info("feature_map: %s, feature_map_depth: %s, entangler_map:%s, nqubits:%s" % (args.feature_map, str(args.feature_map_depth), params['feature_map']['entangler_map'], str(args.nqubits)))
	logging.info("########qubits: %s," % str(args.nqubits))


	if not args.input.endswith(".npz"):
		sample_Total, training_input, test_input, class_labels = load_events_npy(filename=args.input,
																			 training_size=args.training_size,
																			 test_size=args.test_size,
																			 nquits=args.nqubits,
																			 with_pca=args.with_pca,
																			 with_polyfeature=args.withPolyFeature,
																			 with_kernel_pca=False,
																			 with_standardscale=args.with_standardscale,
																			 with_minmax=True,
																			 )
		filename = log_filename.replace('.log', '.npz')
		#save_q_data(filename, training_input, test_input)

	else:
		# LOAD TRAINING AND TEST INPUT VECTORS FROM NPZ
		training_input, test_input = load_npz_data(args.input, args.training_size, args.test_size)

	#print(training_input)

	qsvm = QSVMDev(feature_map, args.batch_size)

	training_dataset, class_to_label = \
					split_dataset_to_data_and_labels(training_input)
	test_dataset = split_dataset_to_data_and_labels(test_input, class_to_label)


	x_train, y_train = shuffle(training_dataset[0], training_dataset[1], random_state=RANDOM_SEED)
	x_test, y_test = shuffle(test_dataset[0], test_dataset[1], random_state=RANDOM_SEED)

	logging.info("training input size:(%s, %s)" % (len(training_dataset[0]),len(training_dataset[0][0])))
	logging.info("testing input size:(%s, %s)" % (len(test_dataset[0]),len(test_dataset[0][0])))

	train_kernel_matrices = []
	test_kernel_matrices = []
	print('######### EVALUATING KERNEL MATRICES ##########\n')
	m=args.evaluation_rounds if args.evaluation_rounds else 3
	for i in range(m):
		print('___________Round {}______________'.format(i + 1))
		matrices = qsvm.get_kernel_matrix(quantum_instance, feature_map, x_train, None)
		train_kernel_matrices.append(matrices)
		test_kernel_matrices.append(qsvm.get_kernel_matrix(quantum_instance, feature_map, x_test, x_train))

	print('######### FINISHED EVALUATING KERNEL MATRICES, AVERAGING AMONG {} ROUNDS ##########\n'.format(m))
	train_kernel_matrix = np.divide(sum(train_kernel_matrices), m)
	test_kernel_matrix  = np.divide(sum(test_kernel_matrices), m)

	print('#'*10 + ' Train kernel matrix '+'#'*10)
	print(np.matrix(train_kernel_matrix))
	print('#'*10 + ' Train kernel matrix dimension '+'#'*10)
	print(train_kernel_matrix.shape)
	print('#'*10 + ' Test kernel matrix '+'#'*10)
	print(np.matrix(test_kernel_matrix))
	print('#'*10 + ' Test kernel matrix dimension '+'#'*10)
	print(test_kernel_matrix.shape)


	svc = svm.SVC(kernel='precomputed', probability=True)

	tune_params = {
				   'C': np.concatenate(((np.arange(1,10,1), np.arange(10,100,10), np.arange(100,1000,100)))),
				   'gamma': np.concatenate(((np.arange(0.01,0.1,0.01), np.arange(0.1,1,0.1))))
				  }

	rs = ShuffleSplit(n_splits=5,test_size=0.2, random_state=RANDOM_SEED)

	clf = GridSearchCV(estimator=svc, param_grid=tune_params, n_jobs=-1, cv=rs, scoring='roc_auc')

	clf.fit(train_kernel_matrix,y_train)

	score = clf.predict_proba(test_kernel_matrix)[:,1]

	logging.info("best_params: %s" % clf.best_params_)

	means = clf.cv_results_['mean_test_score']
	stds = clf.cv_results_['std_test_score']

	for mean, std, params in zip(means, stds, clf.cv_results_['params']):
		logging.info("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))

	#predictions = [round(value) for value in score]
	predictions = clf.predict(test_kernel_matrix)

	roc_auc =  roc_auc_score(y_test, score)
	acc =  accuracy_score(y_test, predictions)

	if args.save_results:
		if not os.path.isdir(args.save_results): os.makedirs(args.save_results)
		result_filename = log_filename.split('/')[-1].replace('.log', '_auc_result.npz')
		result_filename = os.path.join(args.save_results, result_filename)
	else:
		if not os.path.isdir('outputs/sqvm'): os.makedirs('outputs/sqvm')
		result_filename = log_filename.replace('.log', '_auc_result.npz').replace('logs', 'outputs/sqvm')
	np.savez(result_filename,train_kernel_matrix=train_kernel_matrix,test_kernel_matrix=test_kernel_matrix, y_test=y_test,score=score)

	roc_filename=log_filename.split('/')[-1].replace('.log', '_roc.pdf')
	if args.save_roc_plot:
		if not os.path.isdir(args.save_roc_plot): os.makedirs(args.save_roc_plot)
		plotROC(y_scores={'qsvm': score}, y_true=y_test, save_as=roc_filename, save_dir=args.save_roc_plot)
	else:
		plotROC(y_scores={'qsvm': score}, y_true=y_test, save_as=roc_filename, save_dir='roc_plots/qsvm')
		
	logging.info("AUC: %s" % roc_auc)
	logging.info("Accuracy: %s" % acc)
	logging.info("path of the result: %s" % result_filename)

if __name__ == '__main__':
	main(args)
