#!/usr/bin/env bash
for i in 100 200 400 600
do 
    for j in {0..29}
    do
        python run_xgboost.py --input=data/samples_1400-600_signal_mass_point/${i}_events/monohbb_${i}_events_${j}.npy --roc --name=xgboost_monoHbb_${i}events_withoutPolyFeature_${j} --save_roc_plot=roc_plots/xgboost/samples_1400-600_signal_mass_point/monohbb_${i}_events_without_PolyFeature --save_results=outputs/xgboost/samples_1400-600_signal_mass_point/monohbb_${i}_events_without_PolyFeature
    done
done

for i in 100 200 400 600
do 
    for j in {0..29}
    do
        python run_xgboost.py --input=data/samples_1400-600_signal_mass_point/${i}_events/monohbb_${i}_events_${j}.npy --roc --name=xgboost_monoHbb_${i}events_withPolyFeature_${j} --save_roc_plot=roc_plots/xgboost/samples_1400-600_signal_mass_point/monohbb_${i}_events_with_PolyFeature --save_results=outputs/xgboost/samples_1400-600_signal_mass_point/monohbb_${i}_events_with_PolyFeature --withPolyFeature
    done
done