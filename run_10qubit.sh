#!/bin/bash

#nohup python run_vqc.py --input=./data/tth_pca10_100_new_2.npz  --training_size=50 --test_size=50 --nqubits=10  --shots=8192 --optimizer=SPSA --trials=100 --local_var  --feature_map=SecondOrderExpansionDev --feature_map_depth=1 --variational_form=RYRZDev --variational_form_depth=2 --entanglement=linear --loss_method=mean_absolute_error --c0=2 --debug --disable_circuit_caching --backend=ibmq_johannesburg --measurement_nqubits=5 > test_10_qubits1.log&

#nohup python run_vqc.py --input=./data/tth_pca10_100_new_2.npz  --training_size=50 --test_size=50 --nqubits=10  --shots=8192 --optimizer=SPSA --trials=100 --local_var  --feature_map=SecondOrderExpansionDev --feature_map_depth=1 --variational_form=RYRZDev --variational_form_depth=2 --entanglement=linear --loss_method=mean_absolute_error --c0=2  --disable_circuit_caching --backend=ibmq_johannesburg --measurement_nqubits=5 > test_10_qubits1.log&

#submit job
#python run_vqc.py --input=./data/tth_pca10_100_new_2.npz  --training_size=50 --test_size=50 --nqubits=10  --shots=8192 --optimizer=SPSA --trials=100 --local_var  --feature_map=FirstOrderExpansion --feature_map_depth=1 --variational_form=RYRZDev --variational_form_depth=1 --entanglement=linear --loss_method=mean_absolute_error --c0=4 --c2=0.1 --measurement_nqubits=5   --disable_circuit_caching --backend=ibmq_rochester

#python run_vqc.py --input=./data/ttH_100events_qubits10_2.npz  --training_size=50 --test_size=50 --nqubits=10  --shots=8192 --optimizer=SPSA --trials=64 --local_var  --feature_map=FirstOrderExpansion --feature_map_depth=1 --variational_form=RYRZDev --variational_form_depth=1 --entanglement=linear --loss_method=mean_squared_error --c0=3 --c2=0.1  --disable_circuit_caching --measurement_nqubits=5  --theta=./logs/qsvm_variational_classification_2020-03-10_21-42-48_train50_test50_nqubits10_pcaFalse_standscaleFalse_shots8192_trials75_featureFirstOrderExpansion_entanglelinear_ibmq_boeblingen_running_theta.npz --backend=ibmq_boeblingen

#python run_vqc.py --input=./data/ttH_100events_qubits10_3.npz  --training_size=50 --test_size=50 --nqubits=10  --shots=8192 --optimizer=SPSA --trials=50 --local_var  --feature_map=FirstOrderExpansion --feature_map_depth=1 --variational_form=RYRZDev --variational_form_depth=1 --entanglement=linear --loss_method=mean_squared_error --c0=3 --c2=0.1  --disable_circuit_caching --measurement_nqubits=5 --backend=ibmq_boeblingen --theta=./logs/qsvm_variational_classification_2020-03-25_14-00-20_train50_test50_nqubits10_pcaFalse_standscaleFalse_shots8192_trials100_featureFirstOrderExpansion_entanglelinear_ibmq_boeblingen_785_running_theta.npz


python run_vqc.py --input=./data/ttH_100events_qubits10_3.npz  --training_size=50 --test_size=50 --nqubits=10  --shots=8192 --optimizer=SPSA --trials=100 --local_var  --feature_map=FirstOrderExpansion --feature_map_depth=1 --variational_form=RYRZDev --variational_form_depth=1 --entanglement=linear --loss_method=mean_squared_error --c0=3 --c2=0.1  --disable_circuit_caching --measurement_nqubits=5 --backend=ibmq_johannesburg  --theta=./logs/qsvm_variational_classification_2020-04-03_06-42-56_train50_test50_nqubits10_pcaFalse_standscaleFalse_shots8192_trials50_featureFirstOrderExpansion_entanglelinear_ibmq_johannesburg_76_running_theta.npz

# copy outputs
python copy_outputs.py
