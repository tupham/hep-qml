#!/bin/bash

#python run_vqc.py --input=./data/hmumu_twojet_100events_10qubits_2.npz  --training_size=50 --test_size=50 --nqubits=10  --shots=8192 --optimizer=SPSA --trials=100 --local_var  --feature_map=FeatureMap7 --feature_map_depth=1 --variational_form=RYRZDev --variational_form_depth=1 --entanglement=linear --loss_method=mean_squared_error --c0=4 --c2=0.1  --disable_circuit_caching --measurement_nqubits=5 --theta=./logs/qsvm_variational_classification_2020-04-28_08-07-53_train50_test50_nqubits10_pcaFalse_standscaleFalse_shots8192_trials100_featureFeatureMap7_entanglelinear_ibmq_paris_762_running_theta.npz --backend=ibmq_paris

# copy outputs
#python copy_outputs.py
export QISKIT_AQUA_MAX_CIRCUITS_PER_JOB=50 
for i in 4 8
do
    for j in {0..29}
    do
    # python run_qsvm_dev.py --input=data/monohbb_100_events.npy --training_size=100 --test_size=100 --nqubits=$i --shots=8192 --entanglement=full --name=qsvm_monoHbb --feature_map_depth=1  --feature_map=FirstOrderExpansion --cat=tth_hardware --with_pca --backend=qasm_simulator --with_standardscale --save_roc_plot=roc_plots/qsvm/FirstOrderExpansion_100events_depth1_nqubits4_with_without_PolyFeature --save_results=outputs/qsvm/FirstOrderExpansion_100events_depth1_nqubits4_with_without_PolyFeature  --withPolyFeature=True #--save_kernels monohbb_100_events_kernel_matrix
        python run_qsvm.py --input=data/samples_1400-600_signal_mass_point/200_events/monohbb_200_events_${j}.npy --training_size=200 --test_size=200 --nqubits=$i --shots=8192 --entanglement=full --name=qsvm_monoHbb --feature_map_depth=1  --feature_map=SecondOrderExpansion --cat=tth_hardware --with_pca --backend=qasm_simulator --with_standardscale --save_roc_plot=roc_plots/qsvm/maintainance_run --save_results=outputs/qsvm/maintainance_run  #--withPolyFeature #--save_kernels monohbb_100_events_kernel_matrix
        python run_qsvm.py --input=data/samples_1400-600_signal_mass_point/200_events/monohbb_200_events_${j}.npy --training_size=200 --test_size=200 --nqubits=$i --shots=8192 --entanglement=full --name=qsvm_monoHbb --feature_map_depth=1  --feature_map=SecondOrderExpansion --cat=tth_hardware --with_pca --backend=qasm_simulator --with_standardscale --save_roc_plot=roc_plots/qsvm/maintainance_run --save_results=outputs/qsvm/maintainance_run  --withPolyFeature
    done
done
