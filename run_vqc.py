#!/usr/bin/env python

import os
import traceback
import json
import argparse
import sys
import logging
import time
import datetime
import numpy as np

from qiskit import ClassicalRegister, QuantumCircuit, QuantumRegister
from qiskit.aqua.algorithms import QuantumAlgorithm
from qiskit.aqua import AquaError, Pluggable, PluggableType, get_pluggable_class
from qiskit.aqua import QuantumInstance, aqua_globals
#from qiskit.transpiler.coupling import CouplingMap
from qiskit.transpiler import Layout, CouplingMap

# Import measurement calibration functions
from qiskit.ignis.mitigation.measurement import (complete_meas_cal, tensored_meas_cal,
                                                 CompleteMeasFitter, TensoredMeasFitter)

from qiskit.providers.aer import noise
from qiskit.aqua import get_aer_backend
from qiskit import BasicAer
from qiskit import Aer
from qiskit import IBMQ

import Qconfig

from utils.load_data import load_npz_data, load_events_npy, save_npz_data, scale_data

from utils.log_file import get_log_file_name

from algorithms.vqc_dev import VQCDev
from algorithms.second_order_expansion_dev import SecondOrderExpansionDev
#from algorithms.quantum_instance_dev import QuantumInstanceDev
from qiskit.aqua.algorithms import VQC

from qiskit.aqua.components.optimizers import SPSA, COBYLA, P_BFGS, NELDER_MEAD
from qiskit.aqua.components.feature_maps import PauliExpansion, FirstOrderExpansion, SecondOrderExpansion
from qiskit.aqua.components.feature_maps.raw_feature_vector import RawFeatureVector
from qiskit.aqua.components.variational_forms import RYRZ, RY

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug')
parser.add_argument('--input', action='store', help='input data', required=True)
parser.add_argument('--theta', action='store', help='initial theta', required=False)
parser.add_argument('--training_size', action='store', type=int, default=10000, help='number of events to be selected for training')
parser.add_argument('--test_size', action='store', type=int, default=100000, help='number of events to be selected for test')
parser.add_argument('--nqubits', action='store', type=int, default=5, help='number of qubits')
parser.add_argument('--nfeatures', action='store', type=int, default=5, help='number of features')
parser.add_argument('--with_pca', action='store_true', default=False, help='Whether to use PCA')
parser.add_argument('--with_kernel_pca', action='store_true', default=False, help='Whether to use Kernel PCA')
parser.add_argument('--noise_model', action='store', help='noise model', required=False)

parser.add_argument('--ncomponents', action='store', type=int, default=None, help='number of components')
parser.add_argument('--with_standardscale', action='store_true', default=False, help='Whether to use StandardScale')
parser.add_argument('--with_minmaxscale', action='store_true', default=False, help='Whether to use MinMaxScale(-1, 1)')
parser.add_argument('--with_minmaxscale_plus', action='store_true', default=False, help='Whether to use MinMaxScale(0, 1)')
parser.add_argument('--with_normalizer', action='store_true', default=False, help='Whether to use Normalizer')
parser.add_argument('--with_order_data_by_importance', action='store_true', default=False, help='Whether to order data by importance')
parser.add_argument('--random_select', action='store_true', default=False, help='random select inputs')

parser.add_argument('--shots', action='store', type=int, default=1024, help='number of shots')
parser.add_argument('--trials', action='store', type=int, default=100, help='maximum number of trials')
parser.add_argument('--epsilon', action='store', type=float, default=0.0001, help='epsilon')
parser.add_argument('--c0', action='store', type=float, default=0.628, help='SPSA c0')
parser.add_argument('--c1', action='store', type=float, default=0.1, help='SPSA c1')
parser.add_argument('--c2', action='store', type=float, default=0.602, help='SPSA c2')
parser.add_argument('--c3', action='store', type=float, default=0.101, help='SPSA c3')
parser.add_argument('--c4', action='store', type=float, default=0, help='SPSA c4')
parser.add_argument('--max_circuits_per_job', action='store', type=int, default=100, help='maximum number of circuits per job')
parser.add_argument('--feature_map', action='store', default='SecondOrderExpansion', help='feature map method')
parser.add_argument('--entanglement', action='store', default=None, help='entanglement')
parser.add_argument('--variational_form', action='store', default='RYRZ', help='variational form')
parser.add_argument('--variational_form_depth', action='store', type=int, default=4, help='variational form depth')
parser.add_argument('--feature_map_depth', action='store', type=int, default=3, help='feature map depth')
parser.add_argument('--optimizer', action='store', default='SPSA', help='optimizer')
parser.add_argument('--backend', action='store', default='qasm_simulator', help='backend name')
parser.add_argument('--noise_device', action='store', default=None, help='noise device name')
parser.add_argument('--measurement_nqubits', action='store', type=int, default=None, help='number of measurement qubits')
parser.add_argument('--measurement_entanglement_gate', action='store', default='cx', help='measurement entanglement gate')

parser.add_argument('--disable_circuit_caching', action='store_true', default=False, help='Disable circuit caching')
parser.add_argument('--with_error_mitigation', action='store_true', default=False, help='Enable error mitigation')
parser.add_argument('--with_error_mitigation_on_test', action='store_true', default=False, help='Enable error mitigation on test')
parser.add_argument('--with_dev_error_mitigation', action='store_true', default=False, help='Use the dev error mitigation')

parser.add_argument('--name', action='store', default='qsvm_variational_classification', help='problem name')
parser.add_argument('--with_official_run_circuits', action='store_true', default=False, help='Whether to use official run_circuits')
parser.add_argument('--store', action='store', default='default', help='store name')
parser.add_argument('--nworkers', action='store', type=int, default=1, help='number of workers')
parser.add_argument('--minibatch', action='store', type=int, default=-1, help='size of minibatch_size')
parser.add_argument('--loss_method', action='store', default=None, help='loss function')
parser.add_argument('--local_var', action='store_true', default=False, help='using local variational class')
parser.add_argument('--batch', action='store', type=int, default=1, help='using batch mode')

parser.add_argument('--last_n_theta', action='store', type=int, default=10, help='number of last n theta to be saved')

parser.add_argument('--disable_running_theta', action='store_true', default=False, help='Disable save running theta')

args = parser.parse_args()

##Default Params##
default_params = {
        'problem': {'name': 'vqc_classification', 'random_seed': 10598},
        'loss_function':{'name': 'default'},
        'algorithm': {'name': 'vqc', 'batch_mode': True, 'minibatch_size': 2},
        'backend': {'provider': 'qiskit.BasicAer', 'name': 'qasm_simulator', 'shots': 1024, 'skip_calibration': True},
        'optimizer': {'name': 'SPSA', 'max_trials': 30, 'save_steps': 1},
        'variational_form': {'name': 'RYRZ', 'depth': 3, 'num_qubits': 5},
        'feature_map': {'name': 'SecondOrderExpansion', 'depth': 2, 'num_qubits': 5, 'entangler_map': None}
}

##Logging##
def config_logging(params):
    log_filename = get_log_file_name(__file__, params['problem']['name'], args)
    logging.basicConfig(
        # level=logging.INFO,
        level=logging.DEBUG if args.debug else logging.INFO,
        format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
        handlers=[
            logging.FileHandler("{0}".format(log_filename)),
            logging.StreamHandler()
        ])
    return log_filename

def main(args):
    ########preparing###############
    params = default_params

    current_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M-%S')
    if args.with_official_run_circuits:
        params['problem']['name'] = "official" + args.name + "_" + current_time
    else:
        params['problem']['name'] = args.name + "_" + current_time
    params['loss_function']['name']=args.loss_method
    params['algorithm']['batch_mode']=args.batch
    params['algorithm']['minibatch_size']=args.minibatch
    params['problem']['random_seed'] = 10598

    #global seed
    aqua_globals.random_seed = params['problem']['random_seed']

    #feature_map
    params['feature_map']['name'] = args.feature_map
    params['feature_map']['depth'] = args.feature_map_depth
    # if args.entanglement == 'linear' and args.backend not in ['ibmq_16_melbourne', 'ibmqx4']:
    if args.entanglement == 'linear':
       # params['feature_map']['entangler_map'] = [[i, i + 1] for i in range(args.nqubits - 1)]
       params['feature_map']['entangler_map'] = []
       for i in range(0, args.nqubits - 1, 2):
           params['feature_map']['entangler_map'].append([i, i + 1])
       for i in range(0, args.nqubits - 2, 2):
           params['feature_map']['entangler_map'].append([i + 1, i + 2])
       # params['feature_map']['entangler_map'].append([args.nqubits-1,0])

    if params['feature_map']['name'] == 'SecondOrderExpansion': 
       feature_map = SecondOrderExpansionDev(args.nqubits, depth=args.feature_map_depth, entanglement=args.entanglement, entangler_map=params['feature_map']['entangler_map'])
    if params['feature_map']['name'] == 'SecondOrderExpansionDev':
       feature_map = SecondOrderExpansionDev(args.nqubits, depth=args.feature_map_depth, entanglement=args.entanglement, entangler_map=params['feature_map']['entangler_map'])
    if params['feature_map']['name'] == 'FirstOrderExpansion':
       feature_map = FirstOrderExpansion(args.nqubits, depth=args.feature_map_depth)
    if params['feature_map']['name'] == 'FeatureMap7':
       from algorithms.feature_map7 import FeatureMap7
       feature_map = FeatureMap7(num_qubits=args.nqubits, depth=args.feature_map_depth)
    if params['feature_map']['name'] == 'FeatureMap7Dev':
       from algorithms.feature_map_7dev import FeatureMap7Dev
       feature_map = FeatureMap7Dev(num_qubits=args.nqubits, depth=args.feature_map_depth)
    if params['feature_map']['name'] == 'FeatureMap8Dev':
       from algorithms.feature_map8_dev import FeatureMap8Dev
       feature_map = FeatureMap8Dev(num_qubits=args.nqubits, depth=args.feature_map_depth)

    #variational_form
    params['variational_form']['name'] = args.variational_form
    params['variational_form']['depth'] = args.variational_form_depth
    if params['variational_form']['name'] == 'RYRZ':
       var_form = RYRZ(num_qubits=args.nqubits, depth=args.variational_form_depth, entanglement=args.entanglement, entangler_map=params['feature_map']['entangler_map'])
    if params['variational_form']['name'] == 'RY':
       var_form = RY(num_qubits=args.nqubits, depth=args.variational_form_depth, entanglement=args.entanglement, entangler_map=params['feature_map']['entangler_map'])
    if params['variational_form']['name'] == 'RYRZDev':
       from algorithms.ryrz_dev import RYRZDev
       var_form = RYRZDev(num_qubits=args.nqubits, depth=args.variational_form_depth, entanglement=args.entanglement, entangler_map=params['feature_map']['entangler_map'])
    #optimizer
    params['optimizer']['name'] = args.optimizer
    params['optimizer']['max_trials'] = args.trials
    if  params['optimizer']['name'] == 'SPSA':
        optimizer = SPSA(max_trials=args.trials, save_steps=10, c0=args.c0, c1=args.c1, c2=args.c2, c3=args.c3, c4=args.c4, skip_calibration=params['backend']['skip_calibration'])
    if  params['optimizer']['name'] == 'L_BFGS_B':
        optimizer = L_BFGS_B(max_fun=args.trials,epsilon=args.epsilon)
    if  params['optimizer']['name'] == 'COBYLA':
        optimizer = COBYLA(maxiter=args.trials, rhobeg=np.pi*0.1)
    if params['optimizer']['name'] == 'NELDER_MEAD':
        optimizer = NELDER_MEAD(maxiter=args.trials)

    ####################################
    log_filename = config_logging(params)
    logging.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    logging.info("+++++++ execution: %s ++++++++++" % params['problem']['name'])
    logging.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    logging.info("log file: %s" % log_filename)

    logging.info(params)
 
    #
    logging.info("backend: %s, shots: %s" % (args.backend, str(args.shots)))
    logging.info("optimizer: %s, max_trials: %s " % (args.optimizer,str(args.trials)))
    logging.info("variational_form: %s, depth: %s" %(args.variational_form, str(args.variational_form_depth)))
    logging.info("feature_map: %s, feature_map_depth: %s, entangler_map:%s, nqubits:%s" % (args.feature_map, str(args.feature_map_depth), params['feature_map']['entangler_map'], str(args.nqubits)))
    logging.info("input: %s" % args.input)

    if args.nfeatures<=args.nqubits:
       nfeatures = args.nqubits
    else:
       nfeatures = args.nfeatures

    logging.info("########qubits: %s, nfeatures: %s ########" % (str(args.nqubits), str(args.nfeatures)))

    if not args.input.endswith(".npz"):
        sample_Total, training_input, test_input, class_labels = load_events_npy(args.input,
                                                                             args.training_size,
                                                                             args.test_size,
                                                                             nfeatures,
                                                                             args.with_pca,
                                                                             args.with_kernel_pca,
                                                                             args.with_standardscale)
        filename = log_filename.replace('.log', '.npz')
        save_npz_data(filename, training_input, test_input)
    else:
        # training_input, test_input = load_npz_data(args.input, args.training_size, args.test_size)
        training_input, test_input = load_npz_data(args.input)
    
    logging.info(" train size: %s" % {key: training_input[key].shape
                                      for key in training_input.keys()})
    logging.info(" test size: %s" % {key: test_input[key].shape
                                     for key in test_input.keys()})
    training_weight = None
    test_weight     = None

    training_input, test_input, training_weight, test_weight = scale_data(training_input=training_input,
                                                                          test_input=test_input,
                                                                          training_size=args.training_size,
                                                                          test_size=args.test_size,
                                                                          ncomponents=args.ncomponents,
                                                                          with_pca=args.with_pca,
                                                                          with_standardscale=args.with_standardscale,
                                                                          with_minmaxscale=args.with_minmaxscale,
                                                                          with_minmaxscale_plus=args.with_minmaxscale_plus,
                                                                          with_normalizer=args.with_normalizer,
                                                                          with_order_data_by_importance=args.with_order_data_by_importance,
                                                                          random_select=args.random_select,
                                                                          training_weight=training_weight,
                                                                          test_weight=test_weight)
    logging.info("After scaling, train size: %s" % {key: training_input[key].shape
                                                    for key in training_input.keys()})
    logging.info("After scaling,  test size: %s" % {key: test_input[key].shape
                                                    for key in test_input.keys()})

    filename = log_filename.replace('.log', '.npz')
    np.savez(filename, training_input=training_input, test_input=test_input)

    initial_theta = None
    if args.theta:
        theta_data = np.load(args.theta)
        initial_theta = theta_data['best_theta']
        theta_data.close()

    if args.local_var:
        logging.info("local Variational Quantum Classifier method is used; batch: %s; minibatch:%s " %(args.batch, args.minibatch) )
        if args.disable_running_theta:
            running_theta_file = None
        else:
            running_theta_file = log_filename.replace(".log", "_running_theta.npz")
        qsvm_instance = VQCDev(optimizer=optimizer,
                               feature_map=feature_map,
                               var_form=var_form,
                               quantum_instance=None,
                               training_dataset=training_input,
                               test_dataset=test_input,
                               batch_mode=args.batch,
                               minibatch_size=args.minibatch,
                               loss_method=args.loss_method,
                               initial_theta=initial_theta,
                               training_weight=training_weight,
                               test_weight=test_weight,
                               measurement_nqubits=args.measurement_nqubits,
                               measurement_entanglement_gate=args.measurement_entanglement_gate,
                               last_n_theta=args.last_n_theta,
                               running_theta_file=running_theta_file)
    else:
        logging.info("defult method is used")
        qsvm_instance = VQC(optimizer=optimizer,
                                           feature_map=feature_map,
                                           var_form=var_form,
                                           training_dataset=training_input,
                                           test_dataset=test_input,
                                           minibatch_size=args.minibatch,
                                           loss_method=args.loss_method)

    #quantum_instance
    params['backend']['name'] = args.backend
    params['backend']['shots'] = args.shots
    if args.backend.startswith("ibmq"):
        try:
            if Qconfig.APItoken and len(Qconfig.APItoken) > 0:
                IBMQ.enable_account(Qconfig.APItoken, Qconfig.config['url'])
            else:
                IBMQ.load_account()
        except Exception as error:
            logging.error(error)
        provider = IBMQ.get_provider(hub=Qconfig.config['hub'], group=Qconfig.config['group'], project=Qconfig.config['project'])
        backend = provider.get_backend(args.backend)
        #backend = IBMQ.get_backend(args.backend)
        params['backend']['skip_calibration'] = False
    else:
        backend = Aer.get_backend(params['backend']['name'])
        params['backend']['skip_calibration'] = True

    coupling_map = None
    red_cmap = None
    red_noise_model = None
    noise_model = None
    basis_gates = None
    target_qubits = None
    initial_layout = None

    if args.backend.startswith("ibmqx4"):
        if hasattr(qsvm_instance, '_qr'):
            qr = qsvm_instance._qr
            initial_layout = {(qr, 0): 3, (qr, 1): 4, (qr, 2): 2, (qr, 3): 1, (qr, 4): 0}

    if args.backend.startswith("ibmq_16_melbourne"):
        if hasattr(qsvm_instance, '_qr'):
            qr = qsvm_instance._qr
            initial_layout = {(qr, 0): 1, (qr, 1): 2, (qr, 2): 3, (qr, 3): 4, (qr, 4): 10}
        if args.noise_device.startswith("ibm"):
            # target_qubits = [i for i in range(args.nqubits)]
            ##### update it based on hardware
            target_qubits = [3, 4, 2, 1, 0]
            print("target_qubits: %s" % str(target_qubits))
            IBMQ.enable_account(Qconfig.APItoken, Qconfig.config['url'])
            logging.info("noise device: %s" % args.noise_device)
            # device = IBMQ.get_backend(args.noise_device)
            provider = IBMQ.get_provider(hub=Qconfig.config['hub'], group=Qconfig.config['group'], project=Qconfig.config['project'])
            device = provider.get_backend(args.noise_device)
            properties = device.properties()
            coupling_map = device.configuration().coupling_map
            CMAP = CouplingMap(coupling_map)

            red_cmap = CMAP.reduce(target_qubits)

            # Generate an Aer noise model for device
            noise_model = noise.device.basic_device_noise_model(properties)
            red_noise_model = noise.utils.remap_noise_model(noise_model,
                                                            remapping=target_qubits,
                                                            discard_qubits=True)

            basis_gates = red_noise_model.basis_gates
            """
            initial_layout = Layout.from_intlist(target_qubits)

            if hasattr(qsvm_instance, '_qr'):
                qr = qsvm_instance._qr
                # initial_layout = {(qr, 0): 3, (qr, 1): 4, (qr, 2): 2, (qr, 3): 1, (qr, 4): 0}
                initial_layout = {}
                for i in range(len(target_qubits)):
                    initial_layout[(qr, i)] = target_qubits[i]
            """
            initial_layout = None

    if args.backend.startswith("ibmq_boeblingen"):
        if hasattr(qsvm_instance, '_qr'):
            qr = qsvm_instance._qr
            if args.nqubits == 5:
               initial_layout = {(qr, 0): 0, (qr, 1): 1, (qr, 2): 2, (qr, 3): 3, (qr, 4): 8}
            elif args.nqubits == 10:
               initial_layout = {(qr, 0): 0, (qr, 1): 1, (qr, 2): 2, (qr, 3): 3, (qr, 4): 8, (qr, 5): 7, (qr, 6): 6, (qr, 7): 5, (qr, 8): 10, (qr, 9): 11}
    if args.backend.startswith("ibmq_rochester"):
        if hasattr(qsvm_instance, '_qr'):
            qr = qsvm_instance._qr
            if args.nqubits == 5:
               initial_layout = {(qr, 0): 0, (qr, 1): 1, (qr, 2): 2, (qr, 3): 3, (qr, 4): 4}
            elif args.nqubits == 10:
               initial_layout = {(qr, 0): 7, (qr, 1): 8, (qr, 2): 9, (qr, 3): 10, (qr, 4): 11, (qr, 5): 12, (qr, 6): 13, (qr, 7): 14, (qr, 8): 15, (qr, 9): 18}

    if args.backend.startswith("ibmq_johannesburg"):
        if hasattr(qsvm_instance, '_qr'):
            qr = qsvm_instance._qr
            if args.nqubits == 5:
               initial_layout = {(qr, 0): 0, (qr, 1): 5, (qr, 2): 6, (qr, 3): 7, (qr, 4): 8}
            elif args.nqubits == 10:
               initial_layout = {(qr, 0): 0, (qr, 1): 5, (qr, 2): 6, (qr, 3): 7, (qr, 4): 8, (qr, 5): 9, (qr, 6): 14, (qr, 7): 13, (qr, 8): 12, (qr, 9): 11}

    if args.backend.startswith("ibmq_paris"):
        if hasattr(qsvm_instance, '_qr'):
            qr = qsvm_instance._qr
            if args.nqubits == 5:
               initial_layout = {(qr, 0): 0, (qr, 1): 1, (qr, 2): 2, (qr, 3): 3, (qr, 4): 5}
            elif args.nqubits == 10:
               initial_layout = {(qr, 0): 0, (qr, 1): 1, (qr, 2): 2, (qr, 3): 3, (qr, 4): 5, (qr, 5): 8, (qr, 6): 11, (qr, 7): 14, (qr, 8): 13, (qr, 9): 12}

    if args.noise_model:
        nm = np.load(args.noise_model, allow_pickle=True)
        nm_noise = nm['noise_model'].item()
        noise_model = noise.NoiseModel.from_dict(nm_noise)

        coupling_map = nm['coupling_map']
        coupling_map = coupling_map.tolist()        
        CMAP = CouplingMap(coupling_map)
        
        target_qubits = [0,1,2,3,4]
        red_cmap = CMAP.reduce(target_qubits)

        #red_cmap = [[0, 1], [1, 2], [2, 3], [3, 4]]
        red_noise_model = noise.utils.remap_noise_model(noise_model,
                                                        remapping=target_qubits,
                                                        discard_qubits=True)
        basis_gates = red_noise_model.basis_gates

        if hasattr(qsvm_instance, '_qr'):
            qr = qsvm_instance._qr
        #initial_layout = {qr[0]: 0, qr[1]: 1, qr[2]: 2, qr[3]: 3, qr[4]: 4}
        initial_layout = None

    print("red_noise_model: %s" % str(red_noise_model))
    print("basis_gates: %s" % str(basis_gates))
    print("reduced coupling_map: %s" % str(red_cmap))
    print('initial_layout: %s' % str(initial_layout))

    if args.disable_circuit_caching:
        circuit_caching = False
    else:
        circuit_caching = True

    measurement_error_mitigation_cls = None
    cals_matrix_refresh_period = None
    if args.with_error_mitigation:
        measurement_error_mitigation_cls = CompleteMeasFitter
        cals_matrix_refresh_period = 60
    if args.with_dev_error_mitigation:
        from algorithms.quantum_instance_dev import QuantumInstanceDev as QuantumInstance
    else:
        from qiskit.aqua import QuantumInstance

    quantum_instance = QuantumInstance(backend,
                                       shots=params['backend']['shots'],
                                       seed_simulator=params['problem']['random_seed'], 
                                       seed_transpiler=params['problem']['random_seed'],
                                       basis_gates=basis_gates,
                                       coupling_map=red_cmap,
                                       noise_model=red_noise_model,
                                       initial_layout=initial_layout,
                                       circuit_caching=circuit_caching,
                                       measurement_error_mitigation_cls=measurement_error_mitigation_cls,
                                       cals_matrix_refresh_period = cals_matrix_refresh_period)

    if args.with_error_mitigation_on_test:
        measurement_error_mitigation_cls = CompleteMeasFitter
        cals_matrix_refresh_period = 60

    quantum_instance_test = QuantumInstance(backend,
                                            shots=params['backend']['shots'],
                                            seed_simulator=params['problem']['random_seed'],
                                            seed_transpiler=params['problem']['random_seed'],
                                            basis_gates=basis_gates,
                                            coupling_map=red_cmap,
                                            noise_model=red_noise_model,
                                            initial_layout=initial_layout,
                                            circuit_caching=circuit_caching,
                                            measurement_error_mitigation_cls=measurement_error_mitigation_cls,
                                            cals_matrix_refresh_period = cals_matrix_refresh_period)

    qsvm_instance.random_seed = params['problem']['random_seed']

    # result = qsvm_instance.run(quantum_instance)
    qsvm_instance.train(qsvm_instance._training_dataset[0], qsvm_instance._training_dataset[1], quantum_instance=quantum_instance)
    qsvm_instance.test_last_n_theta(qsvm_instance._test_dataset[0], qsvm_instance._test_dataset[1], quantum_instance=quantum_instance_test)
    qsvm_instance.test(qsvm_instance._test_dataset[0], qsvm_instance._test_dataset[1], quantum_instance=quantum_instance_test)
    result = qsvm_instance._ret

    #if not args.local_var:
    #    logging.info("testing success ratio: %s" % str(result['test_success_ratio']))
    #else:
    logging.info("testing success ratio: %s" % str(result['test_success_ratio']))
    logging.info("test predict probs: %s" % result['test_predicted_probs'])
    logging.info("test lable: %s" % result['test_labels'])
    logging.info("test auc: %s" % result['test_auc'])
    logging.info("best theta: %s" % result['opt_params'])

    result_filename = log_filename.replace(".log", "_auc_result.npz")
    np.savez(result_filename,
             test_success_ratio=result['test_success_ratio'],
             test_predicted_probs=result['test_predicted_probs'],
             test_labels=result['test_labels'],
             test_auc=result['test_auc'],
             best_theta=result['opt_params'],
             all_rets=result)

    best_theta_filename = log_filename.replace(".log", "_best_theta.npz")
    np.savez(best_theta_filename, best_theta=result['opt_params'])

if __name__ == '__main__':
    main(args)
