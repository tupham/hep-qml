#!/usr/bin/env python -v

import argparse
import datetime
import logging
import os
import time
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler, MinMaxScaler, Normalizer
from sklearn.svm import SVC
from sklearn.metrics import roc_curve, auc, confusion_matrix, accuracy_score

from qiskit import Aer
from qiskit.aqua import QuantumInstance, aqua_globals
from qiskit.transpiler import Layout, CouplingMap

# Import measurement calibration functions
from qiskit.ignis.mitigation.measurement import (complete_meas_cal, tensored_meas_cal,
                                                 CompleteMeasFitter, TensoredMeasFitter)
from qiskit.compiler import transpile, assemble
from qiskit.providers.aer import noise
from qiskit.aqua import get_aer_backend
from qiskit import BasicAer
from qiskit import Aer
from qiskit import IBMQ

from qiskit.aqua.algorithms import QSVM
from qiskit.aqua.components.feature_maps import PauliExpansion, FirstOrderExpansion, SecondOrderExpansion
from algorithms.second_order_expansion_dev import SecondOrderExpansionDev
import Qconfig

parser = argparse.ArgumentParser()
parser.add_argument('--task_name', action='store', default=None, help='input file')
parser.add_argument('--kernel', action='store', default=None, help='kernel')
parser.add_argument('--degree', action='store', type=int, default=3, help='degrees')
parser.add_argument('--C', action='store', type=float, default=1.0, help='C')
parser.add_argument('--input', action='store', default=None, help='input file', required=True)
parser.add_argument('--output_dir', action='store', default="./logs", help='input file')
parser.add_argument('--pca_components', action='store', type=int, default=None, help='pca components')
parser.add_argument('--training_size', action='store', type=int, default=100, help='number of events to be selected for training')
parser.add_argument('--test_size', action='store', type=int, default=100, help='number of events to be selected for test')
parser.add_argument('--with_minmaxscale', action='store_true', default=False, help='Whether to use MinMaxScale(-1, 1)')
parser.add_argument('--random_select', action='store_true', default=False, help='random select inputs')
parser.add_argument('--save_inputs', action='store_true', default=False, help='Whether to save inputs')

parser.add_argument('--nqubits', action='store', type=int, default=5, help='number of qubits')
parser.add_argument('--feature_map', action='store', default='SecondOrderExpansion', help='feature map method')
parser.add_argument('--entanglement', action='store', default='linear', help='entanglement')
parser.add_argument('--feature_map_depth', action='store', type=int, default=2, help='feature map depth')
parser.add_argument('--backend', action='store', default='statevector_simulator', help='backend name')

parser.add_argument('--noise_device', action='store', default=None, help='noise device name')
parser.add_argument('--with_error_mitigation', action='store_true', default=False, help='Enable error mitigation')

args = parser.parse_args()

logging.basicConfig(level=logging.DEBUG)


def get_train_test_samples(training_input, test_input):
    sample_train = None
    sample_test = None
    for label in training_input:
        if sample_train is None:
            sample_train = training_input[label]
        else:
            sample_train = np.append(sample_train, training_input[label], axis=0)
    for label in test_input:
        if sample_test is None:
            sample_test = test_input[label]
        else:
            sample_test = np.append(sample_test, test_input[label], axis=0)
    np.random.shuffle(sample_train)
    np.random.shuffle(sample_test)
    ncomponents = sample_train[0].shape[0]
    print("train samples: %s, test samples: %s, ncomponents in data: %s" % (sample_train.shape, sample_test.shape, ncomponents))
    return sample_train, sample_test, ncomponents


def load_npz_data(filename, training_size=None, test_size=None, random_select=False, pca_components=None, with_minmaxscale=False):
    """
    load npz data

    :Returns:
             training_input, test_input
    """
    data = np.load(filename, allow_pickle=True)
    training_input = {'input':v for  k,v in np.ndenumerate(data['training_input'])}['input']
    test_input = {'input':v for  k,v in np.ndenumerate(data['test_input'])}['input']
    validate_input = {}
    if 'validate_input' in data:
        validate_input = {'input':v for  k,v in np.ndenumerate(data['validate_input'])}['input']

    #training_input = data['training_input']
    #test_input = data['test_input']

    training_shape = {key: training_input[key].shape
                      for key in training_input.keys()}

    test_shape = {key: test_input[key].shape
                  for key in test_input.keys()}

    validate_shape = {key: validate_input[key].shape
                      for key in validate_input.keys()}

    print("train shape: %s" % training_shape)
    print("test shape: %s" % test_shape)
    print("validate shape: %s" % validate_shape)

    if pca_components:
        sample_train, sample_test, ncomponents_data = get_train_test_samples(training_input, test_input)
        print("PCA is applied")
        # Now reduce number of features to ncomponents
        pca = PCA(n_components=pca_components).fit(sample_train)
        for label in training_input:
            training_input[label] = pca.transform(training_input[label])
        for label in test_input:
            test_input[label] = pca.transform(test_input[label])

    if with_minmaxscale:
        print("MinMaxScaler is applied")
        sample_train, sample_test, ncomponents_data = get_train_test_samples(training_input, test_input)
        # Scale to the range (-1,+1) -np.pi, np.pi
        samples = np.append(sample_train, sample_test, axis=0)
        minmax_scale = MinMaxScaler((-1, 1)).fit(samples)
        for label in training_input:
            training_input[label] = minmax_scale.transform(training_input[label])
        for label in test_input:
            test_input[label] = minmax_scale.transform(test_input[label])
        if validate_input:
            for label in validate_input:
                validate_input[label] = minmax_scale.transform(validate_input[label])

    if random_select:
        print("Random shuffle events for random select")
        training_input = {key: np.random.permutation(training_input[key])
                          for key in training_input.keys()}

        test_input = {key: np.random.permutation(test_input[key])
                      for key in test_input.keys()}

    if training_size:
        print("training_size: %s" % training_size)
        training_input = {key: training_input[key][:training_size] for key in training_input.keys()}

    if test_size:
        print("test_size: %s" % test_size)
        test_input = {key: test_input[key][:test_size] for key in test_input.keys()}

    data.close()

    training_shape = {key: training_input[key].shape
                      for key in training_input.keys()}

    test_shape = {key: test_input[key].shape
                  for key in test_input.keys()}

    validate_shape = {key: validate_input[key].shape
                      for key in validate_input.keys()}

    print("After scale, train shape: %s" % training_shape)
    print("After scale, test shape: %s" % test_shape)
    print("After scale, validate shape: %s" % validate_shape)

    return training_input, test_input, validate_input


def draw_roc_curve(fpr, tpr):
    plt.xlabel("FPR", fontsize=14)
    plt.ylabel("TPR", fontsize=14)
    plt.title("ROC Curve", fontsize=14)
    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    plt.plot(fpr, tpr, color='r--', linewidth=2, label='bdt')


def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]


def draw_circuits(x1, x2, level=2):
    quantum_instance = get_quantum_instance()
    qsvm_instance = get_qsvm_instance()
    circut = qsvm_instance._construct_circuit((x1, x2), feature_map=get_featuremap(args), measurement=True)
    circuit = transpile(circut,
              optimization_level=level,
              backend=quantum_instance.backend)
    print(circuit.depth())
    circuit.draw(filename='test_circuit_qasm_%s.pdf' % level, output='mpl')


def get_quantum_instance():
    aqua_globals.random_seed = 10598

    if args.backend.startswith("ibmq"):
        try:
            IBMQ.enable_account(Qconfig.APItoken, Qconfig.config['url'])
        except:
            pass
        provider = IBMQ.get_provider(hub=Qconfig.config['hub'], group=Qconfig.config['group'], project=Qconfig.config['project'])
        backend = provider.get_backend(args.backend)
        #backend = IBMQ.get_backend(args.backend)
        #params['backend']['skip_calibration'] = False
    else:
        backend = Aer.get_backend(args.backend)
        #params['backend']['skip_calibration'] = True

    coupling_map = None
    red_cmap = None
    red_noise_model = None
    noise_model = None
    basis_gates = None
    target_qubits = None
    initial_layout = None

    if args.noise_device and args.noise_device.endswith(".npz"):
        print("load noise from file: %s" % args.noise_device)
        noise_device = np.load(args.noise_device, allow_pickle=True)
        basis_gates = ['u1', 'u2', 'u3', 'cx', 'id']
        if basis_gates in noise_device.keys():
            basis_gates = noise_device['basis_gates']
        coupling_map = noise_device['coupling_map']
        coupling_map = coupling_map.tolist()
        noise_model = noise_device['noise_model']
        CMAP = CouplingMap(coupling_map)
        noise_model = noise.NoiseModel.from_dict(noise_model.tolist())

        target_qubits = [i for i in range(args.nqubits)]
        red_cmap = CMAP.reduce(target_qubits)
        red_noise_model = noise.utils.remap_noise_model(noise_model,
                                                        remapping=target_qubits,
                                                        discard_qubits=True)

    measurement_error_mitigation_cls = None
    cals_matrix_refresh_period = None
    if args.with_error_mitigation:
        measurement_error_mitigation_cls = CompleteMeasFitter
        cals_matrix_refresh_period = 60

    quantum_instance = QuantumInstance(backend,
                                       shots=8192,
                                       seed_simulator=10598,
                                       seed_transpiler=10598,
                                       basis_gates=basis_gates,
                                       coupling_map=red_cmap,
                                       noise_model=red_noise_model,
                                       initial_layout=initial_layout,
                                       circuit_caching=False,
                                       skip_qobj_validation=False,
                                       measurement_error_mitigation_cls=measurement_error_mitigation_cls,
                                       cals_matrix_refresh_period = cals_matrix_refresh_period)
    return quantum_instance


def get_featuremap(args):
    #feature_map
    entangler_map =None
    if args.entanglement == 'linear':
       # entangler_map = [[i, i + 1] for i in range(args.nqubits - 1)]
       entangler_map = []
       for i in range(0, args.nqubits - 1, 2):
           entangler_map.append([i, i + 1])
       for i in range(0, args.nqubits - 2, 2):
           entangler_map.append([i + 1, i + 2])

    if args.feature_map == 'SecondOrderExpansion':
       feature_map = SecondOrderExpansion(args.nqubits, depth=args.feature_map_depth, entanglement=args.entanglement, entangler_map=entangler_map)
    if args.feature_map == 'SecondOrderExpansionDev':
       feature_map = SecondOrderExpansionDev(args.nqubits, depth=args.feature_map_depth, entanglement=args.entanglement, entangler_map=entangler_map)
    if args.feature_map == 'FirstOrderExpansion':
       feature_map = FirstOrderExpansion(args.nqubits, depth=args.feature_map_depth)
    return feature_map


def get_qsvm_instance():
    feature_map = get_featuremap(args)
    qsvm_instance = QSVM(feature_map=feature_map)
    qsvm_instance.random_seed = 10598
    return qsvm_instance


def qsvm_kernel(x1, x2=None):
    print("x1.shape: %s, x2.shape: %s" % (str(np.array(x1).shape), str(np.array(x2).shape) if x2 is not None else ''))
    print(x1)
    print(x2)
    quantum_instance = get_quantum_instance()
    qsvm_instance = get_qsvm_instance()
    #matrix = qsvm_instance.construct_kernel_matrix(x1, x2, quantum_instance=quantum_instance)
    matrix = qsvm_instance.get_kernel_matrix(quantum_instance=quantum_instance, feature_map=get_featuremap(args), x1_vec=x1, x2_vec=x2)
    #print("matrix.shape: %s" % str(np.array(matrix.shape)))
    return matrix


#xgboost
def run_SVM(training_input, validate_input, test_input, is_validate_test_same, qkernel_matrix_filename):

    import xgboost as xgb

    train_sig = training_input['1'] if '1' in training_input.keys() else training_input['A']
    train_bkg = training_input['0'] if '0' in training_input.keys() else training_input['B']

    val_sig   = validate_input['1'] if '1' in validate_input.keys() else validate_input['A']
    val_bkg   = validate_input['0'] if '0' in validate_input.keys() else validate_input['B']

    test_sig   = test_input['1'] if '1' in test_input.keys() else test_input['A']
    test_bkg   = test_input['0'] if '0' in test_input.keys() else test_input['B']

    train = np.concatenate((train_sig, train_bkg))
    val   = np.concatenate((val_sig  , val_bkg  ))
    test = np.concatenate((test_sig  , test_bkg  ))

    print(train.shape, val.shape, test.shape)
    y_train_cat = np.concatenate((np.ones(len(train_sig), dtype=np.uint8), np.zeros(len(train_bkg), dtype=np.uint8)))
    y_val_cat   = np.concatenate((np.ones(len(val_sig)  , dtype=np.uint8), np.zeros(len(val_bkg)  , dtype=np.uint8)))
    y_test_cat   = np.concatenate((np.ones(len(test_sig)  , dtype=np.uint8), np.zeros(len(test_bkg)  , dtype=np.uint8)))

    train, y_train_cat = unison_shuffled_copies(train, y_train_cat)
    val, y_val_cat = unison_shuffled_copies(val, y_val_cat)
    test, y_test_cat = unison_shuffled_copies(test, y_test_cat)

    #print(train)
    #print(y_train_cat)
    print("train shape: %s" % str(train.shape))
    print("y train shape: %s" % str(y_train_cat.shape))

    """
    print(train[0])
    print(train[1])
    draw_circuits(train[0], train[1], level=0)
    draw_circuits(train[0], train[1], level=1)

    draw_circuits(train[0], train[1], level=2)
    draw_circuits(train[0], train[1], level=3)

    return
    """

    # clf = SVC(gamma='auto')
    # kernel: linear, poly, rbf, sigmoid, precomputed
    ###clf = SVC(gamma='auto', kernel='linear')
    # clf = SVC(gamma='auto', kernel='sigmoid')

    if args.kernel == 'qkernel':
        clf = SVC(gamma='scale', kernel=qsvm_kernel, degree=args.degree, probability=True, C=args.C)
    elif args.kernel == 'qkernel_matrix':
        #clf = SVC(gamma='scale', kernel='precomputed', degree=args.degree, probability=True, C=args.C, decision_function_shape='ovr', shrinking=True)
        clf = SVC(gamma='scale', kernel='precomputed', degree=args.degree, probability=True, C=args.C)
        train_matrix = qsvm_kernel(train)
        val_matrix = qsvm_kernel(val, train)
        if is_validate_test_same:
            test_matrix = val_matrix
            y_test_cat = y_val_cat
        else:
            test_matrix = qsvm_kernel(test, train)
        train_val_test_matrix = {'train': train, 'val': val, 'test': test,
                                 'train_matrix': train_matrix, 'val_matrix': val_matrix, 'test_matrix': test_matrix,
                                 'y_train_cat': y_train_cat, 'y_val_cat': y_val_cat, 'y_test_cat': y_test_cat}

        print("qkernel matrix filename: %s" % (qkernel_matrix_filename))
        np.savez(qkernel_matrix_filename, matrix=train_val_test_matrix)
        train = train_matrix
        val = val_matrix
        test = test_matrix
    else:
        clf = SVC(gamma='scale', kernel=args.kernel, degree=args.degree, probability=True, C=args.C)

    clf.fit(train, y_train_cat)

    print('support_: %s' % clf.support_ )
    print('support vector: %s' % clf.support_vectors_)

    #tscore = clf.predict(train)
    #print('tscore: %s' % tscore)
    tscore = clf.predict_proba(train)
    tscore = tscore[:,1]
    #print('tscore proba: %s' % tscore)
    tpredictions = [round(value) for value in tscore]
    taccuracy = accuracy_score(y_train_cat, tpredictions)
    taucValue = roc_curve(y_train_cat, tscore)

    tfpr, ttpr, _ = roc_curve(y_train_cat, tscore)
    troc_auc = auc(tfpr, ttpr)

    print("Train/Val: %s:%s, Tain AUC: %s, Train Acc: %s" % (train.shape[0], val.shape[0], troc_auc, taccuracy))

    vscore = clf.predict_proba(val)
    vscore = vscore[:,1]
    vpredictions = [round(value) for value in vscore]
    vaccuracy = accuracy_score(y_val_cat, vpredictions)
    vaucValue = roc_curve(y_val_cat, vscore)

    fpr, tpr, _ = roc_curve(y_val_cat, vscore)
    vroc_auc = auc(fpr, tpr)

    print("Train/Val: %s:%s, Val AUC: %s, Acc: %s" % (train.shape[0], val.shape[0], vroc_auc, vaccuracy))

    score = clf.predict_proba(test)
    score = score[:,1]
    predictions = [round(value) for value in score]
    accuracy = accuracy_score(y_test_cat, predictions)
    aucValue = roc_curve(y_test_cat, score)

    fpr, tpr, _ = roc_curve(y_test_cat, score)
    roc_auc = auc(fpr, tpr)

    print("Train/Val: %s:%s, Test AUC: %s, Acc: %s" % (train.shape[0], val.shape[0], roc_auc, accuracy))

    result ={}
    result['test_success_ratio'] =  accuracy
    result['test_success_roc_curve']   = aucValue
    result['test_auc'] = roc_auc
    result['test_predicted_probs'] = score
    result['test_labels'] = y_val_cat
    return result


def run(args):
    training_input, test_input, validate_input = load_npz_data(args.input, args.training_size, args.test_size,
                                                               random_select=args.random_select,
                                                               with_minmaxscale=args.with_minmaxscale,
                                                               pca_components=args.pca_components)

    if args.save_inputs:
        input_file_name = args.task_name + '_input.npz'
        np.savez(input_file_name, training_input=training_input, test_input=test_input, validate_input=validate_input)

    is_validate_test_same = False
    if not validate_input:
        validate_input = test_input
        is_validate_test_same = True

    for key in training_input:
        print("training size %s: %s" % (key, str(training_input[key].shape)))
    for key in test_input:
        print("test size %s: %s" % (key, str(test_input[key].shape)))
    for key in validate_input:
        print("validate size %s: %s" % (key, str(validate_input[key].shape)))

    if args.task_name:
        result_filename = args.task_name + "_bdt_result.npz"
    else:
        if ".npz." in args.input:
            result_filename = args.input.replace(".npz.", "_bdt_result.npz.")
        else:
            result_filename = args.input.replace(".npz", "_bdt_result.npz")

    result_filename = os.path.basename(result_filename)
    current_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d_%H_%M_%S')
    result_filename = result_filename.replace("_bdt_result", "_bdt_result" + current_time)
    result_filename = os.path.join(args.output_dir, result_filename)

    qkernel_matrix_filename = result_filename.replace("_bdt_result", '_qkernel_matrix')
    result = run_SVM(training_input, validate_input, test_input, is_validate_test_same, qkernel_matrix_filename)
    #print(result)

    np.savez(result_filename,
                 test_success_ratio=result['test_success_ratio'],
                 test_predicted_probs=result['test_predicted_probs'],
                 test_labels=result['test_labels'],
                 test_auc=result['test_auc'])



if __name__ == '__main__':
    run(args)

