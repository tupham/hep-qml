
nohup python train_npz.py --input=../data/hmumu_onejet_100_1.npz --kernel=qkernel_matrix --training_size=50 --test_size=50 --task_name=test_tth_pca5_100_e1_boebligen  --feature_map_depth=1 --nqubits=5 --backend=ibmq_beobelingen > test_qkernel_ibmq_boeblingen.log&

#python train_npz.py --input=../data/tth_pca5_100_2.npz --kernel=qkernel_matrix --training_size=50 --test_size=50 --task_name=test_tth_pca5_100_1_statevector_nonoise  --feature_map_depth=1 --nqubits=5 --backend=statevector_simulator
