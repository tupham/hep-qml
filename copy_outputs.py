#!/usr/bin/env python

import os
import subprocess
import traceback


def get_size_from_tag(full_path):
    try:
        if not os.path.exists(full_path):
            return 0
        with open(full_path, 'r') as f:
            size = f.readline()
        size = size.replace('\n', '')
        size = int(size)
        return size
    except:
        print(traceback.format_exc())
    return 0


def copy_file(file_path):
    cmd = "scp -i ssh-keys/id_rsa %s yming@atlas02.chtc.wisc.edu:~/quantum_outputs/" % file_path
    print(cmd)
    cprocess = subprocess.run(cmd, shell=True)
    print(cprocess.returncode)
    print(cprocess.stdout)
    print(cprocess.stderr)
    return cprocess.returncode, cprocess.stdout, cprocess.stderr


def copy_files(path):
    if not os.path.exists(path):
        return

    for file in os.listdir(path):
        full_path = os.path.join(path, file)
        full_path_tag = full_path + '.copy'
        if os.path.isfile(full_path) and not full_path.endswith(".copy"):
            file_size = os.path.getsize(full_path)
            copied_size = get_size_from_tag(full_path_tag)
            if copied_size < file_size:
                status, output, error = copy_file(full_path)
                if status == 0:
                    with open(full_path_tag, 'w') as f:
                        f.write("%s\n" % file_size)
                else:
                    print(output)
                    print(error)


if __name__ == '__main__':
    copy_files('./logs')
    copy_files('./svm/logs')
