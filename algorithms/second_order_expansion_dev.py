import logging
import itertools
import numpy as np

from qiskit.quantum_info import Pauli
from qiskit.qasm import pi
from sympy.core.numbers import NaN

from qiskit.aqua import Operator

from qiskit import QuantumCircuit, QuantumRegister
#from qiskit.aqua.components.feature_maps import PauliZExpansion, self_product, SecondOrderExpansion
from qiskit.aqua.components.feature_maps import PauliZExpansion, self_product, SecondOrderExpansion

from algorithms.data_mapping_dev import self_product_dev


logger = logging.getLogger('SecondOrderExpansionDev')


class SecondOrderExpansionDev(SecondOrderExpansion):
    def __init__(self, feature_dimension, depth=2, entangler_map=None,
                 entanglement='full', data_map_func=self_product_dev):
        super(SecondOrderExpansionDev, self).__init__(feature_dimension, depth, entangler_map,
                                                      entanglement, data_map_func)

    def _build_subset_paulis_string(self, paulis):
        # fill out the paulis to the number of qubits
        temp_paulis = []
        for pauli in paulis:
            len_pauli = len(pauli)
            for possible_pauli_idx in itertools.combinations(range(self._num_qubits), len_pauli):
                string_temp = ['I'] * self._num_qubits
                for idx in range(len(possible_pauli_idx)):
                    string_temp[-possible_pauli_idx[idx] - 1] = pauli[-idx - 1]
                temp_paulis.append(''.join(string_temp))
        # clean up string that can not be entangled.
        final_paulis = []
        entangle_full_map = {}
        for pauli in temp_paulis:
            where_z = np.where(np.asarray(list(pauli[::-1])) != 'I')[0]
            if len(where_z) == 1:
                final_paulis.append(pauli)
            else:
                for src, targ in itertools.combinations(where_z, 2):
                    # key = '%s_%s' % (src, targ)
                    key = (src, targ)
                    entangle_full_map[key] = pauli
        for src, targ in self._entangler_map:
            # key = '%s_%s' % (src, targ)
            key = (src, targ)
            if key in entangle_full_map.keys():
                final_paulis.append(entangle_full_map[key])
        logger.info("Pauli terms include: {}".format(final_paulis))
        return final_paulis
