import logging
import time

from qiskit import __version__ as terra_version
from qiskit import Aer
from qiskit.assembler.run_config import RunConfig
from qiskit.transpiler import Layout

from qiskit.aqua.utils import (run_qobjs, compile_circuits, CircuitCache,
                    get_measured_qubits_from_qobj,
                    build_measurement_error_mitigation_fitter,
                    mitigate_measurement_error)
from qiskit.aqua.utils.backend_utils import (is_aer_provider,
                                  is_ibmq_provider,
                                  is_statevector_backend,
                                  is_simulator_backend,
                                  is_local_backend)

logger = logging.getLogger(__name__)


from qiskit.aqua import QuantumInstance



class QuantumInstanceDev(QuantumInstance):
    def __init__(self, backend, shots=1024, seed=None, max_credits=10,
                 basis_gates=None, coupling_map=None,
                 initial_layout=None, pass_manager=None, seed_transpiler=None,
                 backend_options=None, noise_model=None, timeout=None, wait=5,
                 circuit_caching=True, cache_file=None, skip_qobj_deepcopy=True,
                 skip_qobj_validation=True, measurement_error_mitigation_cls=None,
                 cals_matrix_refresh_period=30):
        super(QuantumInstanceDev, self).__init__(backend=backend, shots=shots, seed=seed, max_credits=max_credits,
                                                 basis_gates=basis_gates, coupling_map=coupling_map,
                                                 initial_layout=initial_layout, pass_manager=pass_manager,
                                                 seed_transpiler=seed_transpiler,
                                                 backend_options=backend_options, noise_model=noise_model,
                                                 timeout=timeout, wait=wait,
                                                 circuit_caching=circuit_caching, cache_file=cache_file,
                                                 skip_qobj_deepcopy=skip_qobj_deepcopy,
                                                 skip_qobj_validation=skip_qobj_validation,
                                                 measurement_error_mitigation_cls=measurement_error_mitigation_cls,
                                                 cals_matrix_refresh_period=cals_matrix_refresh_period)

    def execute(self, circuits, **kwargs):
        logger.info("Dev error mitigation class is used: %s" % self._measurement_error_mitigation_cls)

        qobjs = compile_circuits(circuits, self._backend, self._backend_config, self._compile_config, self._run_config,
                                 show_circuit_summary=self._circuit_summary, circuit_cache=self._circuit_cache,
                                 **kwargs)

        result = run_qobjs(qobjs, self._backend, self._qjob_config, self._backend_options, self._noise_config,
                           self._skip_qobj_validation)

        if self._measurement_error_mitigation_cls is not None:
            if self.maybe_refresh_cals_matrix():
                logger.info("Building calibration matrix for measurement error mitigation.")
                qubit_list = get_measured_qubits_from_qobj(qobjs)
                self._measurement_error_mitigation_fitter = self.build_measurement_error_mitigation_fitter(qobjs, result, qubit_list,
                                                                                          self._measurement_error_mitigation_cls,
                                                                                          self._backend,
                                                                                          self._backend_config,
                                                                                          self._compile_config,
                                                                                          self._run_config,
                                                                                          self._qjob_config,
                                                                                          self._backend_options,
                                                                                          self._noise_config)

        if self._measurement_error_mitigation_fitter is not None:
            result = mitigate_measurement_error(result, self._measurement_error_mitigation_fitter,
                                                self._measurement_error_mitigation_method)

        if self._circuit_summary:
            self._circuit_summary = False

        return result

    def build_measurement_error_mitigation_fitter(self, qobjs, qresult, qubits, fitter_cls, backend,
                                                  backend_config=None, compile_config=None,
                                                  run_config=None, qjob_config=None, backend_options=None,
                                                  noise_config=None):
        """
        print('backend')
        print(backend)
        print(backend.properties())
        print(backend.configuration())
        print('backend_config')
        print(backend_config)
        print('compile_config')
        print(compile_config)
        print('run_config')
        print(run_config)
        print('qjob_config')
        print(qjob_config)
        print('backend_options')
        print(backend_options)
        print('noise_config')
        print(noise_config)
        raise
        """
        sim_backend = Aer.get_backend('qasm_simulator')
        expected_results = run_qobjs(qobjs, sim_backend, qjob_config, backend_options, noise_config={},
                                     skip_qobj_validation=False)
        
