import sys
import numpy as np
import logging

from qiskit import ClassicalRegister, QuantumCircuit, QuantumRegister
from qiskit.tools import parallel_map
from qiskit.tools.events import TextProgressBar
from qiskit.aqua.algorithms import QSVM
from qiskit.aqua import aqua_globals

logger = logging.getLogger(__name__)

class QSVMDev(QSVM):

    def __init__(self, feature_map=None, batch_size=None, training_dataset=None, test_dataset=None):
        super().__init__(feature_map, training_dataset, test_dataset,None)
        self._batch_size = batch_size
        #print('batch size: ', self._batch_size)

    def get_kernel_matrix(self, quantum_instance, feature_map, x1_vec, x2_vec=None):
        """
        Construct kernel matrix, if x2_vec is None, self-innerproduct is conducted.
        Notes:
            When using `statevector_simulator`, we only build the circuits for Psi(x1)|0> rather than
            Psi(x2)^dagger Psi(x1)|0>, and then we perform the inner product classically.
            That is, for `statevector_simulator`, the total number of circuits will be O(N) rather than
            O(N^2) for `qasm_simulator`.
        Args:
            quantum_instance (QuantumInstance): quantum backend with all settings
            feature_map (FeatureMap): a feature map that maps data to feature space
            x1_vec (numpy.ndarray): data points, 2-D array, N1xD, where N1 is the number of data,
                                    D is the feature dimension
            x2_vec (numpy.ndarray): data points, 2-D array, N2xD, where N2 is the number of data,
                                    D is the feature dimension
        Returns:
            numpy.ndarray: 2-D matrix, N1xN2
        """

        if x2_vec is None:
            is_symmetric = True
            x2_vec = x1_vec
        else:
            is_symmetric = False

        is_statevector_sim = quantum_instance.is_statevector

        measurement = not is_statevector_sim
        measurement_basis = '0' * feature_map.num_qubits
        mat = np.ones((x1_vec.shape[0], x2_vec.shape[0]))

        # get all indices
        if is_symmetric:
            mus, nus = np.triu_indices(x1_vec.shape[0], k=1)  # remove diagonal term
        else:
            mus, nus = np.indices((x1_vec.shape[0], x2_vec.shape[0]))
            mus = np.asarray(mus.flat)
            nus = np.asarray(nus.flat)
        #print(mus, nus)
        if is_statevector_sim:
            if is_symmetric:
                to_be_computed_data = x1_vec
            else:
                to_be_computed_data = np.concatenate((x1_vec, x2_vec))

            #  the second x is redundant
            to_be_computed_data_pair = [(x, x) for x in to_be_computed_data]

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("Building circuits:")
                TextProgressBar(sys.stderr)
                
            circuits = parallel_map(QSVM._construct_circuit,
                                    to_be_computed_data_pair,
                                    task_args=(feature_map, measurement, is_statevector_sim),
                                    num_processes=aqua_globals.num_processes)
            results = quantum_instance.execute(circuits)

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug("Calculating overlap:")
                TextProgressBar(sys.stderr)

            offset = 0 if is_symmetric else len(x1_vec)

            ###########compuate overlap##########
            lists = list(zip(mus, nus + offset))
            matrix_elements=[]
            for idx in lists:
                tmp_mat = QSVM._compute_overlap(idx, results,True,0)
                matrix_elements.append(tmp_mat)

            for i, j, value in zip(mus, nus, matrix_elements):
                mat[i, j] = value
                if is_symmetric:
                    mat[j, i] = mat[i, j]
        else:
            for idx in range(0, len(mus), self._batch_size):
                to_be_computed_data_pair = []
                to_be_computed_index = []
                for sub_idx in range(idx, min(idx + self._batch_size, len(mus))):
                    i = mus[sub_idx]
                    j = nus[sub_idx]
                    x1 = x1_vec[i]
                    x2 = x2_vec[j]
                    if not np.all(x1 == x2):
                        to_be_computed_data_pair.append((x1, x2))
                        to_be_computed_index.append((i, j))

                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug("Building circuits:")
                    TextProgressBar(sys.stderr)

                logger.info("length of to_be_computed_data_pair: %s" % len(to_be_computed_data_pair))
                circuits = parallel_map(QSVM._construct_circuit,
                                        to_be_computed_data_pair,
                                        task_args=(feature_map, measurement),
                                        num_processes=aqua_globals.num_processes)

                logger.info("length of circuits: %s" % len(circuits))
 
                results = quantum_instance.execute(circuits)
                #np.savez(res_path+"_"+str(idx)+".npy", results)

                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug("Calculating overlap:")
                    TextProgressBar(sys.stderr)

                matrix_elements=[]
                for idx in range(len(circuits)):
                    tmp_mat = QSVM._compute_overlap(idx, results,is_statevector_sim, measurement_basis)
                    matrix_elements.append(tmp_mat)

                for (i, j), value in zip(to_be_computed_index, matrix_elements):
                    mat[i, j] = value
                    if is_symmetric:
                        mat[j, i] = mat[i, j]

        return mat
