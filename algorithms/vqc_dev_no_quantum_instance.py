import re
import logging
import json
import numpy as np
import functools
import copy
import sklearn

from sklearn.metrics import roc_auc_score, log_loss

from concurrent import futures

from qiskit.aqua._discover import (_discover_on_demand,
                        local_pluggables,
                        PluggableType,
                        get_pluggable_class)

import qiskit

from qiskit.aqua.algorithms.adaptive.vqc.vqc import (cost_estimate, return_probabilities)
#from qiskit._util import local_hardware_info

from qiskit.aqua.algorithms import VQC

logger = logging.getLogger(__name__)


class VQCDev(VQC):

    def __init__(self, optimizer=None, feature_map=None, var_form=None, quantum_instance=None, training_dataset=None, test_dataset=None, batch_mode=True, minibatch_size=-1, loss_method='logloss', training_weight=None, test_weight=None):

        super().__init__(optimizer, feature_map, var_form, training_dataset, test_dataset,None, batch_mode, minibatch_size)

        self._quantum_instance = quantum_instance
        self._loss_method = loss_method
        self._training_weight = None
        self._test_weight = None
   
        #self._round = 0
        #self._qobj_jsons = []
        #self._circuits = {}
        #self._noise_config = {}
        #self._qjob_config = {'timeout': None}
        
        self._ret = {}

    def _get_prediction(self, data, theta):
        """
        Make prediction on data based on each theta.
        Args:
            data (numpy.ndarray): 2-D array, NxD, N data points, each with D dimension
            theta ([numpy.ndarray]): list of 1-D array, parameters sets for variational form
        Returns:
            numpy.ndarray or [numpy.ndarray]: list of NxK array
            numpy.ndarray or [numpy.ndarray]: list of Nx1 array
        """
        if self._quantum_instance.is_statevector:
            raise ValueError('Selected backend "{}" is not supported.'.format(
                self._quantum_instance.backend_name))

        predicted_probs = []
        predicted_labels = []
        circuits = {}
        circuit_id = 0

        num_theta_sets = len(theta) // self._var_form.num_parameters
        theta_sets = np.split(theta, num_theta_sets)

        for theta in theta_sets:
            for datum in data:
                circuit = self.construct_circuit(datum, theta, measurement=True)
                # circuit.draw(filename='test_circuit.pdf', output='mpl')
                # raise
                circuits[circuit_id] = circuit
                circuit_id += 1

        #print(circuits.values())
        results = self._quantum_instance.execute(list(circuits.values()))

        circuit_id = 0
        predicted_probs = []
        predicted_labels = []
        for theta in theta_sets:
            counts = []
            for datum in data:
                counts.append(results.get_counts(circuits[circuit_id]))
                circuit_id += 1
            #logger.info("+++++++counts:%s, _num_classes:%s" %(counts,self._num_classes))

            probs = return_probabilities(counts, self._num_classes)
            #probs = self.return_probabilities(counts, self._num_classes) 
            predicted_probs.append(probs)
            predicted_labels.append(np.argmax(probs, axis=1))

        if len(predicted_probs) == 1:
            predicted_probs = predicted_probs[0]
        if len(predicted_labels) == 1:
            predicted_labels = predicted_labels[0]

        return predicted_probs, predicted_labels

    def test(self, data, labels, quantum_instance=None, minibatch_size=-1, params=None):
        """Predict the labels for the data, and test against with ground truth labels.
        Args:
            data (numpy.ndarray): NxD array, N is number of data and D is data dimension
            labels (numpy.ndarray): Nx1 array, N is number of data
            quantum_instance (QuantumInstance): quantum backend with all setting
            minibatch_size (int): the size of each minibatched accuracy evalutation
            params (list): list of parameters to populate in the variational form
        Returns:
            float: classification accuracy
        """
        # minibatch size defaults to setting in instance variable if not set
        minibatch_size = minibatch_size if minibatch_size > 0 else self._minibatch_size

        batches, label_batches, weight_batches = self.batch_data(data, labels, minibatch_size, self._test_weight)
        self.batch_num = 0
        if params is None:
            params = self.optimal_params
        total_cost = 0
        total_correct = 0
        total_samples = 0

        self._quantum_instance = self._quantum_instance if quantum_instance is None else quantum_instance

        pred_probs=[]
        pred_labels=[]
        b_labels=[]
        for batch, label_batch, weight_batch in zip(batches, label_batches, weight_batches):
            predicted_probs, predicted_labels = self._get_prediction(batch, params)
            total_cost += self._cost_function(predicted_probs, label_batch, weight_batch)
            total_correct += np.sum((np.argmax(predicted_probs, axis=1) == label_batch))
            total_samples += label_batch.shape[0]
            int_accuracy = np.sum((np.argmax(predicted_probs, axis=1) == label_batch)) / label_batch.shape[0]
            pred_probs.append(predicted_probs)
            pred_labels.append(predicted_labels)
            b_labels.append(label_batch)
            logger.debug('Intermediate batch accuracy: {:.2f}%'.format(int_accuracy * 100.0))

        total_accuracy = total_correct / total_samples
        logger.info('Accuracy is {:.2f}%'.format(total_accuracy * 100.0))
        self._ret['testing_accuracy'] = total_accuracy
        self._ret['test_success_ratio'] = total_accuracy
        self._ret['testing_loss'] = total_cost / len(batches)

        pred_labels = np.array(pred_labels).flatten()
        pred_probs  = np.array(pred_probs).reshape(-1,2)
        b_labels= np.array(b_labels).flatten()

        auc = roc_auc_score(b_labels, pred_probs[:,1])
        self._ret['test_predicted_probs'] = pred_probs
        self._ret['test_predicted_labels'] = pred_labels
        self._ret['test_auc'] = auc
        self._ret['test_labels'] = b_labels

        return total_accuracy

    def train(self, data, labels, quantum_instance=None, minibatch_size=-1):
        """Train the models, and save results.
        Args:
            data (numpy.ndarray): NxD array, N is number of data and D is dimension
            labels (numpy.ndarray): Nx1 array, N is number of data
            quantum_instance (QuantumInstance): quantum backend with all setting
            minibatch_size (int): the size of each minibatched accuracy evalutation
        """
        self._quantum_instance = self._quantum_instance if quantum_instance is None else quantum_instance
        minibatch_size = minibatch_size if minibatch_size > 0 else self._minibatch_size
        self._batches, self._label_batches, self._weight_batches = self.batch_data(data, labels, minibatch_size, self._training_weight)
        self._batch_index = 0

        logger.debug("+++self._batches: %s " % len(self._batches))
        logger.debug("+++self._label_batches: %s" % len(self._label_batches))

        if self.initial_point is None:
            self.initial_point = self.random.randn(self._var_form.num_parameters)
        
        self._eval_count = 0
        self._ret = self.find_minimum(initial_point=self.initial_point,
                                      var_form=self.var_form,
                                      cost_fn=self._cost_function_wrapper,
                                      optimizer=self.optimizer)

        if self._ret['num_optimizer_evals'] is not None and self._eval_count >= self._ret['num_optimizer_evals']:
            self._eval_count = self._ret['num_optimizer_evals']
        self._eval_time = self._ret['eval_time']

        logger.info('Optimization complete in {} seconds.\nFound opt_params {} in {} evals'.format(
            self._eval_time, self._ret['opt_params'], self._eval_count))
        self._ret['eval_count'] = self._eval_count

        del self._batches
        del self._label_batches
        del self._batch_index

        self._ret['training_loss'] = self._ret['min_val']

    def _cost_function_wrapper(self, theta):
        batch_index = self._batch_index % len(self._batches)
        predicted_probs, predicted_labels = self._get_prediction(self._batches[batch_index], theta)
 
        total_cost = []
        total_accuracy = []
        total_auc = []
        if not isinstance(predicted_probs, list):
            # logger.info("predicted_probs is not list")
            predicted_probs = [predicted_probs]

        # logger.info("predicted_probs: %s" % str(predicted_probs))
        for i in range(len(predicted_probs)):
            curr_cost = self._cost_function(predicted_probs[i], self._label_batches[batch_index], self._weight_batches[batch_index])
            total_cost.append(curr_cost)
            if self._callback is not None:
                self._callback(self._eval_count,
                               theta[i * self._var_form.num_parameters:(i + 1) * self._var_form.num_parameters],
                               curr_cost,
                               self._batch_index)
            self._eval_count += 1

            # calculate accuracy and auc
            accuracy = np.sum((np.argmax(predicted_probs[i], axis=1) == self._label_batches[batch_index])) / self._label_batches[batch_index].shape[0]
            auc = roc_auc_score(self._label_batches[batch_index], predicted_probs[i][:, 1])
            total_accuracy.append(accuracy)
            total_auc.append(auc)

        logger.info("batch index: %s, total_cost: %s, total_accuracy: %s, total_auc: %s" % (self._batch_index, total_cost, total_accuracy, total_auc))
        self._batch_index += 1
        logger.debug('Intermediate batch cost: {}'.format(sum(total_cost)))
        return total_cost if len(total_cost) > 1 else total_cost[0]

    # Breaks data into minibatches. Labels are optional, but will be broken into batches if included.
    def batch_data(self, data, labels=None, minibatch_size=-1, weight=None):
        label_batches = None
        weight_batches = None

        if 0 < minibatch_size < len(data):
            batch_size = min(minibatch_size, len(data))
            if labels is not None:
                if weight is not None:
                    shuffled_samples, shuffled_labels, shuffled_weight = shuffle(data, labels, weight, random_state=self.random)
                    label_batches = np.array_split(shuffled_labels, batch_size)
                    weight_batches = np.array_split(shuffled_weight, batch_size)
                else:
                    shuffled_samples, shuffled_labels = shuffle(data, labels, random_state=self.random)
                    label_batches = np.array_split(shuffled_labels, batch_size)
            else:
                shuffled_samples = shuffle(data, random_state=self.random)
            batches = np.array_split(shuffled_samples, batch_size)
        else:
            batches = np.asarray([data])
            label_batches = np.asarray([labels])
            weight_batches = np.asarray([weight])
        return batches, label_batches, weight_batches

    def _cost_function(self, predicted_probs, labels, weight):
        """
        Calculate cost of predicted probability of ground truth label based on
        cross entropy function.
        Args:
            predicted_probs (numpy.ndarray): NxK array
            labels (numpy.ndarray): Nx1 array
        Returns:
            float: cost
        """

        logging.info("local loss %s is used" % self._loss_method)
        y_pred = predicted_probs[:,1]

        if self._loss_method == 'log_loss':
            total_loss = sklearn.metrics.log_loss(labels, y_pred, weight)
        elif self._loss_method == 'brier_score_loss':
            total_loss = sklearn.metrics.brier_score_loss(labels, y_pred, weight)
        elif self._loss_method == 'cohen_kappa_score':
            total_loss = 1 - sklearn.metrics.cohen_kappa_score(labels, y_pred, weight)
        elif self._loss_method == 'hamming_loss':
            total_loss = sklearn.metrics.hamming_loss(labels, y_pred, weight)
        elif self._loss_method == 'hinge_loss':
            total_loss = sklearn.metrics.hinge_loss(labels, y_pred, weight)
        elif self._loss_method == 'zero_one_loss':
            total_loss = sklearn.metrics.zero_one_loss(labels, y_pred, weight)
        elif self._loss_method == 'explained_variance_score':
            total_loss = 1 - sklearn.metrics.explained_variance_score(labels, y_pred, weight)
        elif self._loss_method == 'mean_absolute_error':
            total_loss = sklearn.metrics.mean_absolute_error(labels, y_pred, weight)
        elif self._loss_method == 'mean_squared_error':
            total_loss = sklearn.metrics.mean_squared_error(labels, y_pred, weight)
        elif self._loss_method == 'mean_squared_log_error':
            total_loss = sklearn.metrics.mean_squared_log_error(labels, y_pred, weight)
        elif self._loss_method == 'median_absolute_error':
            total_loss = sklearn.metrics.median_absolute_error(labels, y_pred, weight)
        elif self._loss_method == 'r2_score':
            total_loss = 1 - sklearn.metrics.r2_score(labels, y_pred, weight)
        elif self._loss_method == 'coverage_error':
            total_loss = sklearn.metrics.coverage_error(labels, y_pred, weight)
        else:
            total_loss = cost_estimate(predicted_probs, labels)

        # logging.debug("predicted_probs:%s, labels:%s" %(predicted_probs,labels)) 
        #total_loss_logloss =  sklearn.metrics.log_loss(labels, y_pred) #log_loss(labels, y_pred)
        #total_loss_new = total_loss_logloss * 0.5 + total_loss * (total_loss_logloss / total_loss) * 0.5
        #logging.info("logloss: %s, loss_origin: %s, total_loss_new: %s" % (total_loss_logloss, total_loss, total_loss_new))

        logging.info("total_loss:%s" % total_loss)
        return total_loss

