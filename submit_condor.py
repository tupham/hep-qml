#!/usr/bin/env python

import os
from datetime import datetime
import time
from argparse import ArgumentParser
from condor.condor import condor_booklist, createScript
import json

def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Process input rootfiles into numpy arrays for Hmumu XGBoost analysis.")
    #parser.add_argument('-i', '--inputdir', action='store', default='inputs', help='Directory that contains ntuples.')
    parser.add_argument('--config_file', action='store', help='Path to configuration file')
    parser.add_argument('--recovering', action='store_true', help='Whether to check and recover missing output files from a previous run')
    return  parser.parse_args()


def arrange_samplelist(inputdir):
    samples=[]
    if not os.path.isdir(inputdir):
        raise SystemExit("Input directory does not exist. Exiting!!!")
    for filename in os.listdir(inputdir):
        if not '.npy' in filename: continue
        samples.append("%s/%s" % (inputdir, filename))
    return samples

def arrange_configuration(argument_dict):
    configuration = {}
    inputdir = argument_dict['inputdir']
    #if len(inputdirs) == 0: 
    #    raise SystemExit('Input directory empty. Exiting!!!')
    configs = argument_dict['configuration']
    for argument in configs:
        value  = configs[argument]
        if isinstance(value, int):
            configuration[argument] = value
        elif isinstance(value, str) and value in ["True", "False"]:
            if value =='True': configuration[argument] = ""
        elif isinstance(value, str) and len(value) != 0:
            configuration[argument] = value
    configuration['inputs'] = [os.path.join(inputdir,file) for file in os.listdir(inputdir) if '.npy' in file]
    return configuration

def enumerate_missing_files(outputdir, inputdir):
    output_files = [f for f in os.listdir(outputdir) if f.endswith('.npz')]
    input_files = [f for f in os.listdir(inputdir) if f.endswith('.npy')]
    missing_input_files = input_files.copy()
    output_ids = [file.split('_')[7] for file in output_files]
    for input_file in input_files:
        input_id = input_file.split('_')[3].replace('.npy', '')
        if input_id in output_ids: missing_input_files.remove(input_file)
    return missing_input_files

def main():

    args=getArgs()
    with open(args.config_file) as f:
        configs = json.load(f)
    CONDA_PREFIX = configs['CONDA_PREFIX']
    INITDIR = configs['INITDIR']
    configuration = arrange_configuration(configs)
    
    if not os.path.isdir(configuration['--save_results']): os.makedirs(configuration['--save_results'])
    #print(configuration)
    script = \
f"""#!/bin/bash

input=$1

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('{configs['CONDA_PREFIX']}/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "{CONDA_PREFIX}/etc/profile.d/conda.sh" ]; then
        . "{CONDA_PREFIX}/etc/profile.d/conda.sh"
    else
        export PATH="{CONDA_PREFIX}/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

conda activate /afs/cern.ch/work/t/tupham/qml

cd {INITDIR}

"""
    python_command = f"""python run_qsvm.py --input $input """
    for argument in configuration:
        if not argument == 'inputs': python_command += argument + ' ' + f'{configuration[argument]}' + ' '

    script += \
f"""echo {python_command} 
{python_command}
#python script.py 
"""
                

    Executable = createScript('condor/scripts/submit_qsvm_0.sh', script)

    #with open('data/inputs_config.json') as f:
    #    config = json.load(f)

    #sample_list = [sample for sample in os.listdir(inputdir) if '.npy' in sample]     #config['sample_list']
    event_number = max(configs['configuration']['--training_size'], configs['configuration']['--test_size'])

    inputs = configuration['inputs']
    if args.recovering:
        missing_input_files = enumerate_missing_files(outputdir=configs['configuration']['--save_results'], inputdir=configs['inputdir'])
        inputs = [os.path.join(configs['inputdir'], missing_input_file) for missing_input_file in missing_input_files]

    if len(inputs) == 0:
        raise SystemExit('NO SAMPLE PRESENT EXITING!!!')
    #for event_number in sample_list:
    condor_list = condor_booklist(Executable=Executable, JobName='qsvm')
    condor_list.set_initialdir(INITDIR)
    condor_list.initialdir_in_arguments()
    condor_list.set_stream_output(True)
    condor_list.set_stream_error(True)
    if event_number < 400: 
        JobFlavour = 'workday'
    elif 400 <= event_number < 600:
        JobFlavour = 'tomorrow'
    else:
        JobFlavour = 'testmatch'
    condor_list.set_JobFlavour(JobFlavour)
    
    condor_list.add_Argument(inputs)

    condor_list.summary('Basic')
    time.sleep(10)
    condor_list.submit()

    # create a copy of json config file in outputs to recover missing files later
    
    with open(os.path.join(configuration['--save_results'], 'QSVM_config.json'), 'w', encoding='utf-8') as f:
        json.dump(configs, f, ensure_ascii=False, indent=4)

    
if __name__=='__main__':
    main()

