
from collections import OrderedDict

from qiskit.result import Result
from qiskit import qobj
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import os


def from_result_to_dict(result):
    """
    convert result to dict

    :param result: ExperimentResult or Result object.
    :returns:
            dict
    """

    if isinstance(result, Result):
        ret = {}
        for key, value in result.__dict__.items():
            if callable(value) or key.startswith('__'):
                pass
            if key == 'results':
                ret[key] = OrderedDict((k, from_result_to_dict(v))
                                       for k, v in value.items())
            else:
                ret[key] = value
        return ret
    else:
        ret = dict((key, value) for key, value in result.__dict__.items()
                   if not callable(value) and not key.startswith('__'))
        return ret


def from_dict_to_result(result_dict):
    """
    Convert dict to result

    :param result_dict: dict
    :returns:
             Result object.
    """
    # Prepare the experiment results.
    experiment_results = []
    for circuitname, value in result_dict['results'].items():
        circuit = {'header': {'name': circuitname},
                   'success': value['status'] == 'DONE',
                   'status': value['status'],
                   'shots': None,
                   'data': value['data']
                   }
        experiment_results.append(qobj.ExperimentResult(**circuit))

    qobj_result = {'backend_name': result_dict['backend_name'],
                   'backend_version': 'TODO',
                   'qobj_id': result_dict['job_id'],
                   'job_id': result_dict['job_id'],
                   'success': result_dict['status'] == 'COMPLETE',
                   'status': result_dict['status'],
                   'results': experiment_results
                   }
    result = Result(qobj.Result(**qobj_result))
    return result


def plot_roc_curve(y_scores, y_true, show=False, save_roc=None):

    if not isinstance(y_scores, dict):
        print('y_score should be a dict with method names as keys!')
    else:
        plt.figure(figsize=(4.5, 4.5))
        lw = 2
        plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle='--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel("True Positive Rate")
        plt.ylabel("False Positive Rate")
        plt.title("Receiver Operating Characteristic")
        for model in y_scores.keys():
            if not len(y_scores[model]) == len(y_true):
                print('Dimention unmatched!')
                pass
            else:
                fpr, tpr, threshold = roc_curve(y_true, y_scores[model])
                auc_score = auc(fpr, tpr)
                plt.plot(fpr, tpr, lw=lw, label=model + " (AUC = {0:.2f})".format(auc_score))
        plt.legend(loc = "lower right")
        if show: plt.show()
        if isinstance(save_roc, str):
            if not os.path.isdir('bdt_plots'): os.makedirs('bdt_plots')
            plt.savefig('bdt_plots'+ '/'+ save_roc+'.eps')

    # plot multiple roc_curves from a dict of y_scores and true y

