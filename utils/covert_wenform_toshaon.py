
import sys
import numpy as np

filename = sys.argv[1]
print(filename)
save_path = filename.replace(".npz", "_sun.npz")

data = np.load(filename)
training_input = {'0': data['training_input_A'],
                  '1': data['training_input_B']}

test_input = {'0': data['test_input_A'],
              '1': data['test_input_B']}
data.close()

print(training_input)
np.savez(save_path, training_input=training_input, test_input=test_input)
