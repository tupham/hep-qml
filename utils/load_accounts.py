
from utils import Qconfig
from qiskit import IBMQ

def load_ibmq_accounts():
    qx_config = {
                 "APItoken": Qconfig.APItoken,
                 "url": Qconfig.config['url']}
    print('Qconfig loaded from %s.' % Qconfig.__file__)
    IBMQ.save_account(qx_config['APItoken'], qx_config['url'])
    IBMQ.load_accounts()

def enable_ibmq_accounts():
    print('Qconfig loaded from %s.' % Qconfig.__file__)

    APItoken = Qconfig.APItoken
    url = Qconfig.config['url']
    other_config = {
        'hub': Qconfig.config['hub'],
        'group': Qconfig.config['group'],
        'project': Qconfig.config['project']
    }
    IBMQ.enable_account(APItoken, url, **other_config)
