import sys
import logging
import time
import functools

from concurrent import futures
import numpy as np
import qiskit
from qiskit import compile as q_compile
from qiskit.backends.jobstatus import JobStatus
from qiskit.backends import JobError
from qiskit.qobj import qobj_to_dict

from qiskit_aqua.algorithmerror import AlgorithmError
from qiskit_aqua.utils import summarize_circuits

from utils.stores import mysql_store
from utils.result_utils import from_dict_to_result

logger = logging.getLogger(__name__)


def local_run_circuits(circuits, backend, execute_config, qjob_config={}, name=None,
                 max_circuits_per_job=sys.maxsize, show_circuit_summary=False, store='default'):
    """
    An execution wrapper with Qiskit-Terra, with job auto recover capability.
    The autorecovery feature is only applied for non-simulator backend.
    This wraper will try to get the result no matter how long it costs.
    Args:
        circuits (QuantumCircuit or list[QuantumCircuit]): circuits to execute
        backend (str): name of backend
        execute_config (dict): settings for qiskit execute (or compile)
        qjob_config (dict): settings for job object, like timeout and wait
        max_circuits_per_job (int): the maximum number of job, default is unlimited but 300
                is limited if you submit to a remote backend
        show_circuit_summary (bool): showing the summary of submitted circuits.
    Returns:
        Result: Result object
    Raises:
        AlgorithmError: Any error except for JobError raised by Qiskit Terra
    """

    if not isinstance(circuits, list):
        circuits = [circuits]

    my_backend = None
    try:
        my_backend = qiskit.Aer.get_backend(backend)
    except KeyError:
        my_backend = qiskit.IBMQ.get_backend(backend)

    chunks = int(np.ceil(len(circuits) / max_circuits_per_job))
    circuits_ids = {}
    for i in range(chunks):
        sub_circuits = circuits[i * max_circuits_per_job:(i + 1) * max_circuits_per_job]
        num_circuits = len(sub_circuits)
        logger.info("start compiling")
        qobj = q_compile(sub_circuits, my_backend, **execute_config)
        logger.info("finished compiling")
        qobj_payload = qobj_to_dict(qobj, version='1.0.0')
        logger.info("got qobj dict")

        id = mysql_store.save_circuits(qobj_payload, num_circuits, backend, execute_config, qjob_config, name,
                                       max_circuits_per_job, show_circuit_summary, store)
        circuits_ids[id] = {'circuits': sub_circuits, 'status': JobStatus.QUEUED}

    i = 0
    start_running_time = None
    end_running_time = None
    running_time = None
    clean_time = None
    while True:
        status_statistics = get_circuits_status_statistics(circuits_ids, store)

        if start_running_time == None and 'running' in status_statistics.keys():
            start_running_time = time.time()
            clean_time = time.time()
        if end_running_time == None and 'complete' in status_statistics.keys():
            end_running_time = time.time()
        if running_time == None and start_running_time and end_running_time:
            running_time = int(end_running_time - start_running_time) + 20  # two sleep
            if running_time < 120:
                running_time = 120

        if running_time and (time.time() - clean_time > running_time * 3):
            #clean_running_circuits(circuits_ids, store, running_time * 3)
            pass

        if i == 10:
            logger.info("Circuits status statistics: %s" % status_statistics)
            i = 0
        time.sleep(10)
        i += 1
        if 'complete' in status_statistics and len(status_statistics.keys()) == 1:
            logger.info("All circuits finished. Circuits status statistics: %s" % status_statistics)
            break

    return get_circuits_results(circuits_ids, store)

def get_circuits_status_statistics(circuits_ids, store):
    """
    Get circuits status statistics

    Returns:
        True if all of them are done; otherwise False
    """
    ids = [id for id in circuits_ids.keys()]
    states = mysql_store.get_status(ids, store)
    status_statistics = {}
    for id in states:
        if states[id] not in status_statistics:
            status_statistics[states[id]] = 1
        else:
            status_statistics[states[id]] += 1
    return status_statistics


def get_circuits_results(circuits_ids, store):
    """
    Get circuits result

    Returns:
        Result: Result object
    """
    ids = [id for id in circuits_ids.keys()]
    results = mysql_store.get_results(ids, store)

    results = [from_dict_to_result(v) for v in results.values()]
    
    if len(results) != 0:
        result = functools.reduce(lambda x, y: x + y, results)
    else:
        result = None
    return result


def clean_running_circuits(circuits_ids, store, max_running_time):
    """
    Clean circuits which have been running for long time

    Returns:
        Result: Result object
    """
    ids = [id for id in circuits_ids.keys()]
    #mysql_store.clean_running_circuits(ids, store, max_running_time)
