#!/usr/bin/env python -v

import csv
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn.preprocessing import StandardScaler, MinMaxScaler, Normalizer
from sklearn.decomposition import PCA
from sklearn.ensemble import ExtraTreesClassifier
import matplotlib.pyplot as plt
import pandas as pd
#from parameters import RANDOM_SEED
RANDOM_SEED = 1756
import logging
logger = logging.getLogger(__name__)


def save_npz_data(filename, training_input, test_input):
	"""
	Save data to file
	"""
	logger.info("save training_input and test_input to %s" % filename)
	# train_input = {'0': training_input['0'],
	#                '1':training_input['1']}
	# t_input = {'0': test_input['0'],
	#            '1': test_input['1']}
	np.savez(filename, training_input=training_input, test_input=test_input)


def load_npz_data(filename, training_size=None, test_size=None):
	"""
	load npz data

	:Returns:
			 training_input, test_input
	"""
	data = np.load(filename, encoding = 'latin1', allow_pickle = True)
	# UNPACK VALUES FROM DATA FILES, INCLUDING 'training_input' and 'test_input'
	training_input = {'input': value for index, value in np.ndenumerate(data['training_input'])}['input']
	test_input = {'input': value for index, value in np.ndenumerate(data['test_input'])}['input']

	training_weight, test_weight = None, None
	if ('training_weight' in data.files and 'test_weight' in data.files):
		training_weight = {'input': v for k, v in np.ndenumerate(data['training_weight'])}['input']
		test_weight = {'input': v for k, v in np.ndenumerate(data['test_weight'])}['input']
	else:
		logging.warning("training_weight and test_weight are not loaded")

	# training_input = data['training_input']
	# test_input = data['test_input']

	# IF TRAINING_SIZE OR TEST_SIZE IS GIVEN, TRUNCATE EACH SET AFTER THE GIVEN SIZE
	if training_size:
		training_input = {key: training_input[key][:training_size] for key in training_input.keys()}
		if training_weight is not None:
			training_weight = {key: training_weight[key][:training_size] for key in training_weight.keys()}

	if test_size:
		test_input = {key: test_input[key][:test_size] for key in test_input.keys()}
		if test_weight is not None:
			test_weight = {key: test_weight[key][:test_size] for key in test_weight.keys()}

	data.close()

	return training_input, test_input #, training_weight, test_weight

def load_events_npy(filename, nquits, with_pca=True, with_kernel_pca=False, with_standardscale=False, with_minmax=True,
				PLOT_DATA=False,training_size=None, test_size=None, with_polyfeature=False, minmax=[-1,1]):

	logger.info("load data from file: %s" % filename)

	events = np.load(filename)

	data = events[:,:-1]
	target = events[:,-1]
	class_labels = [r'A', r'B']

	sample_train, sample_test, label_train, label_test = train_test_split(data, target,
																		  test_size=0.5,
																		  random_state=RANDOM_SEED,
																		  stratify = target)

	print("sample_train: %s; sample_test %s" % (sample_train.shape, sample_test.shape))

	if training_size and training_size>len(sample_train):
		print('Training_size ({}) is larger than the number of available events ({}), changing training to {}'.format(training_size,len(sample_train), len(sample_train)))
	elif training_size and training_size<len(sample_train):
		sample_train, throwaway_sample, label_train, throwaway_label = train_test_split(sample_train, label_train,
																					train_size = training_size,
																					random_state = RANDOM_SEED,
																					stratify = label_train)
	
	if test_size and test_size>len(sample_test):
		print('Training_size ({}) is larger than the number of available events ({}), changing training to {}'.format(test_size,len(sample_test), len(sample_test)))
	elif test_size and test_size<len(sample_test):
		sample_test, throwaway_sample, label_test, throwaway_label = train_test_split(sample_test, label_test,
																					train_size = test_size,
																					random_state = RANDOM_SEED,
																					stratify = label_test)

	if with_polyfeature or nquits>sample_train.shape[1]:
		from sklearn.preprocessing import PolynomialFeatures
		poly = PolynomialFeatures(2,interaction_only=True,include_bias=False)
		pf = poly.fit(sample_train)
		sample_train = pf.transform(sample_train)
		sample_test = pf.transform(sample_test)


	if with_standardscale:
		# Now we standarize for gaussian around 0 with unit variance
		std_scale = StandardScaler().fit(sample_train)
		sample_train = std_scale.transform(sample_train)
		sample_test = std_scale.transform(sample_test)

	if with_kernel_pca and nquits<sample_train.shape[1]:
		from sklearn.decomposition import KernelPCA
		#pca = KernelPCA(n_components=nquits, kernel='rbf', gamma=10).fit(sample_train)
		pca = KernelPCA(n_components=nquits, kernel='poly', gamma=10).fit(sample_train)
		sample_train = pca.transform(sample_train)
		sample_test = pca.transform(sample_test)
		with_pca = False
		logger.info("############# with_kernel_pca is being used#######################")

	if with_pca and nquits<sample_train.shape[1]:
		# Now reduce number of features to number of qubits
		pca = PCA(n_components=nquits).fit(sample_train)
		sample_train = pca.transform(sample_train)
		sample_test = pca.transform(sample_test)
		logger.info("############# with_pca is being used#######################")


	if with_minmax:
		# Scale to the range (-1,+1)
		samples = np.append(sample_train, sample_test, axis=0)
		minmax_scale = MinMaxScaler((minmax[0], minmax[1])).fit(samples)
		sample_train = minmax_scale.transform(sample_train)
		sample_test = minmax_scale.transform(sample_test)
		logger.info("###########with_minmax is being used ################")

	# Pick training size number of samples from each distro
	training_input = {key: (sample_train[label_train == k, :])
					  for k, key in enumerate(class_labels)}
	test_input = {key: (sample_test[label_test == k, :])
				  for k, key in enumerate(class_labels)}
	logger.warning(" train size: %s" % {key: training_input[key].shape
									 for key in training_input.keys()})
	logger.warning(" test size: %s" % {key: test_input[key].shape
									for key in test_input.keys()})


	"""training_input_size = [training_input[key].shape[0] for key in training_input.keys()]
	test_input_size = [test_input[key].shape[0] for key in test_input.keys()]
	#print(training_input_size)

	train_size = min(training_size, *training_input_size) if training_size else min(*training_input_size)
	if training_size and training_size > train_size:
		print(("training size(%s) is bigger than the available events," % training_size)
					  + "updating training_size to %s" % train_size)
	for key in training_input.keys():
		if train_size == training_input[key].shape[0]: training_input[key] = shuffle(training_input[key],
																					 random_state=RANDOM_SEED)
		else: training_input[key], throwaway = train_test_split(training_input[key], train_size=train_size,
																shuffle=True, random_state=RANDOM_SEED)


	testing_size = min(test_size, *test_input_size) if test_size else min(*test_input_size)
	if test_size and test_size > testing_size:
		print(("test size(%s) is bigger than the available events," % test_size)
					  + "updating test_size to %s" % testing_size)
	for key in test_input.keys():
		if testing_size == test_input[key].shape[0]: test_input[key] = shuffle(test_input[key],
																					 random_state=RANDOM_SEED)
		else: test_input[key], throwaway = train_test_split(test_input[key], train_size=testing_size,
																shuffle=True, random_state=RANDOM_SEED)"""

	"""if PLOT_DATA:
		for k in range(0, 2):
			plt.scatter(sample_train[label_train == k, 0][:train_size],
						sample_train[label_train == k, 1][:train_size])

		plt.title("PCA dim. reduced tth lep2 dataset")
		plt.show()"""

	return sample_train, training_input, test_input, class_labels

def get_train_test_samples(training_input, test_input):
	sample_train = None
	sample_test = None
	for label in training_input:
		if sample_train is None:
			sample_train = training_input[label]
		else:
			sample_train = np.append(sample_train, training_input[label], axis=0)
	for label in test_input:
		if sample_test is None:
			sample_test = test_input[label]
		else:
			sample_test = np.append(sample_test, test_input[label], axis=0)
	np.random.shuffle(sample_train)
	np.random.shuffle(sample_test)
	ncomponents = sample_train[0].shape[0]
	logger.info("train samples: %s, test samples: %s, ncomponents in data: %s" % (sample_train.shape, sample_test.shape, ncomponents))
	return sample_train, sample_test, ncomponents


def order_data_by_importance(training_input, test_input):
	X = None
	y = None
	i = -1
	for label in training_input:
		i += 1
		X_sub = np.concatenate((training_input[label], test_input[label]))
		y_sub = np.asarray([i] * len(X_sub))

		if X is None:
			X = X_sub
			y = y_sub
		else:
			X = np.concatenate((X, X_sub))
			y = np.concatenate((y, y_sub))

	forest = ExtraTreesClassifier(n_estimators=10, random_state=0, min_samples_split=10)

	forest.fit(X, y)
	importances = forest.feature_importances_
	importances_dict = {k: i for k, i in enumerate(importances)}
	sorted_importances_dict = sorted(importances_dict.items(), key=lambda kv: kv[1], reverse=True)
	sorted_importances_index = [k for k, v in sorted_importances_dict]
	for label in training_input:
		training_input[label] = training_input[label][:, sorted_importances_index]
	for label in test_input:
		test_input[label] = test_input[label][:, sorted_importances_index]
	return training_input, test_input


def scale_data(training_input, test_input, training_size, test_size, ncomponents=None, with_pca=False,
			   with_standardscale=False, random_select=False, with_minmaxscale=False, with_normalizer=False,
			   with_minmaxscale_plus=False, with_order_data_by_importance=False,
			   training_weight=None, test_weight=None):

	if with_standardscale:
		logger.info("StandardScaler is applied")
		sample_train, sample_test, ncomponents_data = get_train_test_samples(training_input, test_input)
		# Now we standarize for gaussian around 0 with unit variance
		std_scale = StandardScaler().fit(sample_train)
		for label in training_input:
			training_input[label] = std_scale.transform(training_input[label])
		for label in test_input:
			test_input[label] = std_scale.transform(test_input[label])

	if ncomponents and with_pca:
		sample_train, sample_test, ncomponents_data = get_train_test_samples(training_input, test_input)
		if ncomponents == ncomponents_data:
			logger.info("Data has the sampe number of compoents(%s) as required(%s), skip PCA" % (ncomponents_data, ncomponents))
		else:
			logger.info("PCA is applied")
			# Now reduce number of features to ncomponents
			pca = PCA(n_components=ncomponents).fit(sample_train)
			for label in training_input:
				training_input[label] = pca.transform(training_input[label])
			for label in test_input:
				test_input[label] = pca.transform(test_input[label])

	if with_minmaxscale:
		logger.info("MinMaxScaler is applied")
		sample_train, sample_test, ncomponents_data = get_train_test_samples(training_input, test_input)
		# Scale to the range (-1,+1) -np.pi, np.pi
		samples = np.append(sample_train, sample_test, axis=0)
		minmax_scale = MinMaxScaler((-1, 1)).fit(samples)
		for label in training_input:
			training_input[label] = minmax_scale.transform(training_input[label])
		for label in test_input:
			test_input[label] = minmax_scale.transform(test_input[label])

	if with_minmaxscale_plus:
		logger.info("MinMaxScaler_plus is applied")
		sample_train, sample_test, ncomponents_data = get_train_test_samples(training_input, test_input)
		# Scale to the range (-1,+1) -np.pi, np.pi
		samples = np.append(sample_train, sample_test, axis=0)
		minmax_scale = MinMaxScaler((0, 1)).fit(samples)
		for label in training_input:
			training_input[label] = minmax_scale.transform(training_input[label])
		for label in test_input:
			test_input[label] = minmax_scale.transform(test_input[label])

	if with_normalizer:
		logger.info("Normalizer is applied")
		sample_train, sample_test, ncomponents_data = get_train_test_samples(training_input, test_input)
		# Scale to the range (-1,+1) -np.pi, np.pi
		samples = np.append(sample_train, sample_test, axis=0)
		normalizer = Normalizer().fit(samples)
		for label in training_input:
			training_input[label] = normalizer.transform(training_input[label])
		for label in test_input:
			test_input[label] = normalizer.transform(test_input[label])

	if with_order_data_by_importance:
		logger.info("Order data by importance")
		training_input, test_input = order_data_by_importance(training_input, test_input)

	training_input_size = [training_input[key].shape[0] for key in training_input.keys()]
	if training_size > 0 and training_size > min(training_input_size):
		logger.error(("training size(%s) is bigger than the available events," % training_size)
					 + "not enough events, quit")
		raise Exception("Not enought events")
	test_input_size = [test_input[key].shape[0] for key in test_input.keys()]
	if test_size > 0 and test_size > min(test_input_size):
		logger.error(("test size(%s) is bigger than the available events," % test_size)
					 + "not enough events, quit")
		raise Exception("Not enought events")

	if random_select:
		logger.info("Random shuffle events for random select")
		for key in training_input.keys():
			p = np.random.permutation(len(training_input[key]))
			training_input[key] = training_input[key][p]
			if training_weight is not None:
				training_weight[key] = training_weight[key][p]

		for key in test_input.keys():
			p = np.random.permutation(len(test_input[key]))
			test_input[key] = test_input[key][p]
			if test_weight is not None:
				test_weight[key] = test_weight[key][p]

	# select data
	if training_size > 0:
		# training_input = {key: training_input[key][:training_size] for key in training_input.keys()}
		for key in training_input.keys():
			if key == '1':
				training_input[key] = training_input[key][:training_size]
				if training_weight is not None:
					training_weight[key] = training_weight[key][:training_size]
			else:
				training_input[key] = training_input[key][:training_size]
				if training_weight is not None:
					training_weight[key] = training_weight[key][:training_size]

	if test_size > 0:
		# test_input = {key: test_input[key][:test_size] for key in test_input.keys()}
		for key in test_input.keys():
			if key == '1':
				test_input[key] = test_input[key][:test_size]
				if test_weight is not None:
					test_weight[key] = test_weight[key][:test_size]
			else:
				test_input[key] = test_input[key][:test_size]
				if test_weight is not None:
					test_weight[key] = test_weight[key][:test_size]

	return training_input, test_input, training_weight, test_weight
