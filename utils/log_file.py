
import os
import time
import datetime
import random

def get_log_file_name(file, name, args):
    log_dir = os.path.join(os.path.dirname(os.path.realpath(file)), 'logs')
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    log_filename = "%s_train%s_test%s_nqubits%s_pca%s_standscale%s_shots%s_trials%s_feature%s_entangle%s_%s_%s.log" % (name,
                                                                                                                    args.training_size,
                                                                                                                    args.test_size,
                                                                                                                    args.nqubits,
                                                                                                                    args.with_pca,
                                                                                                                    args.with_standardscale,
                                                                                                                    args.shots,
                                                                                                                    args.trials,
                                                                                                                    args.feature_map,
                                                                                                                    args.entanglement,
                                                                                                                    args.backend,
                                                                                                                    random.randint(1, 1000))
    return os.path.join(log_dir, log_filename)

