#!/usr/bin/env bash

# COMMENT OUT STUFF YOU DON'T WANT

wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh

sh Miniconda2-latest-Linux-x86_64.sh

conda config --add channels https://conda.anaconda.org/NLeSC

conda config --add channels conda-forge

# if you want to create the environment inside miniconda's directory:
conda create -c conda-forge --name env root=6 python=3.6 mkl numpy scipy matplotlib scikit-learn h5py pandas

# if you want to specify the directory of the conda env: 
conda create -c conda-forge --prefix={directory} root=6 python=3.6 mkl numpy scipy matplotlib scikit-learn h5py pandas

# to remove the long prefix name:
conda config --set env_prompt '({name})'

# activate conda env install in miniconda's directory
conda activate env

# activate conda env from a different directory:
# conda activate {directory}

pip install qiskit root_pandas uproot tabulate datetime