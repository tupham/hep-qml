import argparse, os, numpy as np
from utils.parameters import plot_multiple_ROC

parser = argparse.ArgumentParser()
parser.add_argument('--inputdirs', required=True, nargs='+', action='store', help='Directories of input npz files')
parser.add_argument('--categories', required=False, nargs='+', action='store', help='categories to plot')
parser.add_argument('--save_roc_plot', action='store',required=False,help='Specify the directory to save roc plots')
parser.add_argument('--save_as',action='store', required=False,help='Sepcify name to save roc plot')

args = parser.parse_args()

def main(args):
    print('#'*20 + ' PLOTTING ROC CURVES FROM '+ '#'*20)
    no_of_npz_files = 0
    plot_data = {}
    roc_filename = 'combined_roc'
    for inputdir in args.inputdirs:
        print(inputdir)
        if not os.path.isdir(inputdir): 
            print('Input directory does not exist!!!')
            continue
        for npz_file_path in sorted([inputdir+'/'+file for file in os.listdir(inputdir) if file.endswith('.npz')]):
            npz = np.load(npz_file_path)
            if not 'score' in npz.files and 'y_test' in npz.files: 
                print('score and y_test are not in npz file keys' )
                continue
            if no_of_npz_files < len(args.categories): 
                method = args.categories[no_of_npz_files]
            else:
                method = 'category_{}'.format(no_of_npz_files)
            plot_data[method] = {'score': npz['score'], 'y_true': npz['y_test']}
            roc_filename+='_'+method
            no_of_npz_files+=1
    
    save_dir = 'roc_plots/combined_roc_plots' if not args.save_roc_plot else args.save_roc_plot
    
    if not os.path.isdir(save_dir): os.makedirs(save_dir)
    if args.save_as: roc_filename = args.save_as
    plot_multiple_ROC(rocs = plot_data, save_dir=save_dir, save_as=roc_filename)

    return

if __name__=='__main__':
    main(args)
