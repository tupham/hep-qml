#!/bin/bash

python run_vqc.py --input=./data/tth_pca10_100_new_2.npz  --training_size=50 --test_size=50 --nqubits=10  --shots=8192 --optimizer=SPSA --trials=100 --local_var  --feature_map=FirstOrderExpansion --feature_map_depth=1 --variational_form=RYRZDev --variational_form_depth=1 --entanglement=linear --loss_method=mean_absolute_error --c0=4 --c2=0.1 --measurement_nqubits=5   --disable_circuit_caching --backend=ibmq_boeblingen

